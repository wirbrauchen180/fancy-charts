#ifndef CALCULATORVIEW_H
#define CALCULATORVIEW_H

#include <QGraphicsView>

class CalculatorView : public QGraphicsView
{
public:
    CalculatorView(QWidget *parent = nullptr);

    void wheelEvent(QWheelEvent *event);

private:
    qreal mNumScheduledScalings;

    void scalingTime(qreal x);
    void animFinished();
};

#endif // CALCULATORVIEW_H
