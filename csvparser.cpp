#include <algorithm>

#include <QDebug>
#include <QFile>
#include <QRegularExpression>
#include <QTextStream>
#include <QTimeZone>

#include "csvparser.h"

CSVParser::CSVParser(QString format)
{
	QRegularExpression dataRegexp("^\\{(.*)\\}(([;,](high|low|open|close|volume)){1,5})[;,]?$");

	auto match = dataRegexp.match(format);

	if( match.hasMatch() )
    {
		datetimeFormat = match.captured(1);

        QVector<QPair<int, DataOrder>> dataOrder;
		dataOrder.push_back(QPair<int, DataOrder>(match.captured(2).indexOf("open"),     Open));
		dataOrder.push_back(QPair<int, DataOrder>(match.captured(2).indexOf("high"),     High));
		dataOrder.push_back(QPair<int, DataOrder>(match.captured(2).indexOf("low"),       Low));
		dataOrder.push_back(QPair<int, DataOrder>(match.captured(2).indexOf("close"),   Close));
		dataOrder.push_back(QPair<int, DataOrder>(match.captured(2).indexOf("volume"), Volume));

        std::sort(std::begin(dataOrder), std::end(dataOrder), [](QPair<int, DataOrder> &a, QPair<int, DataOrder> &b){return a.first < b.first;});

        for(auto it = dataOrder.begin(); it != dataOrder.end(); ++it)
        {
            if( (*it).first != -1 )
                order.push_back((*it).second);
        }

        if(order.empty() || (!order.contains(Open) && !order.contains(High) && !order.contains(Low) && !order.contains(Close)))
        {
            errors.push_back(QString("Invalid format %1").arg(format));
        }

    }
    else
    {
        errors.push_back(QString("Invalid format %1").arg(format));
    }

    qDebug() << errors;
}

void CSVParser::parse(QString fileNames, bool ignoreFirstLine)
{
    bars.clear();
    int string_idx = 0, file_idx = 0, total_strings = 0;
    QStringList files = fileNames.split(";");

    for(QString &fileName : files)
    {
        if(errors.empty())
        {
            Bar b;

            QFile inputFile(fileName);
            if (inputFile.open(QIODevice::ReadOnly))
            {
               ++file_idx;
               QTextStream in(&inputFile);
               emit fileChanged(file_idx);
               emit progressChanged(0);
               string_idx = 0, total_strings = 0;

               while (!in.atEnd())
               {
                  in.readLine();
                  ++total_strings;
               }

               bars.reserve(bars.size() + total_strings);

               inputFile.reset();
               in.reset();

               while (!in.atEnd())
               {
                  QString line = in.readLine();

                  if(ignoreFirstLine && !string_idx)
                  {
                      ++string_idx;
                      continue;
                  }

                  ++string_idx;

                  auto list = line.split(QRegularExpression("[,;]"));

                  QString datestring = list.at(0);
				  //datestring.append(" 00:00:00");
				  QDateTime dt = QDateTime::fromString(datestring, datetimeFormat);
				  dt.setTimeZone(QTimeZone::Initialization::UTC);

                  if(!dt.isValid())
				  {
                      errors.push_back(QString("Invalid datetime string: %1 for format: %2").arg(datestring, datetimeFormat));
                      break;
                  }
                  else
                      list.pop_front();

                  for(int d = Open; d <= Volume; ++d )
                  {
                      bool ok = true;
                      double value = 0;

                      int idx = order.indexOf(DataOrder(d));

                      if(idx != -1)
                      {
                          value = list.at(idx).toDouble(&ok);

                          if(!ok)
                          {
                              errors.push_back(QString("Invalid number: %1 in string: %2").arg(list.at(idx)).arg(string_idx));
                              break;
                          }

                          switch(DataOrder(d))
                          {
                            case Open:
                              b.Open = value;
                              break;
                            case Close:
                              b.Close = value;
                              break;
                            case Low:
                              b.Low = value;
                              break;
                            case High:
                              b.High = value;
                              break;
                            case Volume:
                              b.Volume = value;
                              break;
                            default: break;
                          }
                      }
                  }

                  b.datetime = dt;

                  if(errors.empty())
                  {
                      bars.push_back(b);
                      emit progressChanged(100 * string_idx / total_strings);
                  }
                  else
                      break;
               }

               inputFile.close();
            }
            else
            {
                errors.push_back(QString("Can't open the file: ").append(fileName));
            }
        }
    }

    emit parseFinished();    
    qDebug()  << bars.size();
}

bool CSVParser::checkErrors()
{
    return !errors.empty();
}

QString CSVParser::errorsString()
{
    QTextStream ss;
    QString result;
    ss.setString(&result);

    for(QString &s : errors)
    {
        ss << s << Qt::endl;
    }

    return result;
}
