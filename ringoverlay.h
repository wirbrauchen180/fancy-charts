#ifndef RINGOVERLAY_H
#define RINGOVERLAY_H

#include <QGraphicsEllipseItem>

#include "arrow.h"
#include "label.h"

enum OverlayType
{
    kDegrees,
    kDays
};

typedef QPair<QGraphicsLineItem*, Label*> LabeledTick;

class RingOverlay : public QGraphicsItemGroup
{
public:
    RingOverlay(OverlayType type, qreal radius, qreal shift, int numberOfArrows = 0, bool connectArrows = false, qreal startAngle = 0, qreal angleSpan = 360, QGraphicsItem *parent = nullptr);

    void clearLabels(QTransform t = {});
    void adjustLabelsPos(QTransform t = {});

private:
    qreal mRadius;
    qreal mShift;
    QVector<LabeledTick> mTicks;
    QVector<Arrow*> mArrows;
    QVector<QGraphicsLineItem*> mConnectors;
    OverlayType mType;
    QGraphicsEllipseItem *mEllipse;

    void createTicks();
};

#endif // RINGOVERLAY_H
