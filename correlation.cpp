#include "correlation.h"

#include <algorithm>
#include <cmath>
#include <QDebug>
#include <QTextStream>
#include <QFile>
#include <QtConcurrent/QtConcurrent>

void ReplaceByRanks(QVector<double> &data)
{
    QVector<double> cData = data;
    QVector<double> pData = data;

    std::sort(std::begin(cData), std::end(cData));
    auto last = std::unique(std::begin(cData), std::end(cData));
    cData.erase(last, std::end(cData));

    std::size_t rank = 0;

    std::for_each(std::begin(cData), std::end(cData), [cData, pData, &data, &rank](double &a)
    {
        std::size_t cnt = std::count(std::begin(pData), std::end(pData), a);
        std::size_t n = cnt + rank;
        double new_rank = (n * (n + 1) - rank * (rank + 1) )/ 2.0 / cnt;
        rank = n;

        std::replace(std::begin(data), std::end(data), a, new_rank);

        if(a == cData.back())
            rank= 0;
    });
}

double GetCorrelationOneRow(QVector<double> &x, QVector<double> &y, CorrelationType type)
{
    Q_ASSERT(x.size() == y.size() || x.size());
    double result = 0;

    if(type == kPearson)
    {
        double sum_x = std::accumulate(std::begin(x), std::end(x), 0.0);
        double sum_y = std::accumulate(std::begin(y), std::end(y), 0.0);
		double sum_xx = std::accumulate(std::begin(x), std::end(x), 0.0, [](double &&sum, double x_i){return sum + x_i * x_i;});
        double sum_xy = std::inner_product(std::begin(x), std::end(x), std::begin(y), 0.0 );
		double sum_yy = std::accumulate(std::begin(y), std::end(y), 0.0, [](double &&sum, double y_i){return sum + y_i * y_i;});
        int n = x.size();

        result = (n * sum_xy - sum_x * sum_y)/(std::pow((n * sum_xx - sum_x * sum_x)*(n * sum_yy - sum_y * sum_y), 0.5));
    }
    else if(type == kSpearman)
    {
        auto a = x, b = y;

        ReplaceByRanks(x);
        ReplaceByRanks(y);

        QVector<double> d;

        std::transform(std::begin(x), std::end(x), std::begin(y), std::back_inserter(d), std::minus<double>());

		double sum_dd = std::accumulate(std::begin(d), std::end(d), 0.0, [](double &&sum, double d_i){return sum + d_i * d_i;});
        int n = x.size();

        result = 1 - (6 * sum_dd / (n * (n * n - 1)));
    }

    return result;
}

double averagePrice(Bar &bar, int priceType)
{
    double price = 0;
    int count = 0;

    if(!(priceType & kAverage))
    {
        if(priceType & kOpen)
            price = bar.Open;
        if(priceType & kClose)
            price = bar.Close;
        if(priceType & kHigh)
            price = bar.High;
        if(priceType & kLow)
            price = bar.Low;

        return price;
    }
    if(priceType & kOpen)
    {
        price += bar.Open;
        ++count;
    }
    if(priceType & kClose)
    {
        price += bar.Close;
        ++count;
    }
    if(priceType & kHigh)
    {
        price += bar.High;
        ++count;
    }
    if(priceType & kLow)
    {
        price += bar.Low;
        ++count;
    }

    return price/count;
}

double GetCorrelation(QVector<Bar> &x, QVector<Bar> &y, CorrelationType type, int priceType)
{
    QVector<double> a, b;
    double result = 0;
    int count = 0;

    if(priceType & kAverage)
    {
        std::transform(std::begin(x), std::end(x), std::back_inserter(a), [priceType](Bar &bar){return averagePrice(bar, priceType);});
        std::transform(std::begin(y), std::end(y), std::back_inserter(b), [priceType](Bar &bar){return averagePrice(bar, priceType);});

        result = GetCorrelationOneRow(a, b, type);
    }
    else
    {
        int steps = kOpen;
        double correlationSum = 0;

        do
        {   if(steps & priceType)
            {
                a.clear();
                b.clear();
                std::transform(std::begin(x), std::end(x), std::back_inserter(a), [steps](Bar &bar){return averagePrice(bar, steps);});
                std::transform(std::begin(y), std::end(y), std::back_inserter(b), [steps](Bar &bar){return averagePrice(bar, steps);});

                correlationSum += GetCorrelationOneRow(a, b, type);
                ++count;
            }

            steps <<= 1;
        }
        while( steps < kAverage);

        result = correlationSum / count;
    }

    return result;
}

QVector<Correlation> GetCorrelationMovingWindow(QVector<Bar> &bars, std::size_t firstBar, std::size_t diff, std::size_t windowSize,
                                                int priceType, CorrelationType correlationType, bool onlyForward)
{
    QVector<Correlation> result;
    QVector<Bar> x, y;

    if(!onlyForward)
        firstBar = 0;

    for(std::size_t i = firstBar; i <= bars.size() - 2 * windowSize - diff; ++i)
    {
        x = bars.mid(i, windowSize);
        y = bars.mid(i + diff + windowSize, windowSize);

        double correlation = GetCorrelation(x, y, correlationType, priceType);

	result.push_back(Correlation{correlation, bars.at(i).datetime, bars.at(i + diff + windowSize).datetime, i, i + diff + windowSize});
    }

    return result;
}

void next_step(int number, int depth, QVector<QVector<int>> &result, QVector<int> *vec, int min_value = 1)
{
    for(int i = number; i >= min_value; --i)
    {
        if(i == number)
        {
            QVector<int> r;

            for(int j = 0; j < depth; ++j)
            {
                r.push_back(vec->at(j));
            }

            r.push_back(i);
            result.push_back(r);
        }
        else
        {
            if(number - i >= min_value)
            {
                (*vec)[depth] = i;
                next_step(number - i, depth + 1, result, vec, min_value);
            }
        }
    }
}

QVector<QVector<int>> SplitWindow(std::size_t windowSize, std::size_t minSize)
{
    QVector<QVector<int>> result;
    QVector<int> res(256);
    next_step(windowSize, 0, result, &res, minSize);

    return result;
}

struct MappedParameters
{
    QVector<Bar> x;
    QVector<Bar> y;
    QVector<int> split;
    CorrelationType type;
    int priceType;
};

double GetCorrelationWithParameters(const MappedParameters &mp)
{
    double correlationSum = 0;
    int idx = 0;
    std::size_t startIdx = 0;

    QVector<Bar> a, b;

    for(int p : mp.split)
    {
        a = mp.x.mid(startIdx, p);
        b = mp.y.mid(startIdx, p);

        double corr = GetCorrelation(a, b, mp.type, mp.priceType);

        correlationSum += (!(idx%2) ?  corr * p : -corr * p);
        ++idx;
        startIdx += p;
    }

    return correlationSum/mp.x.size();
}



QPair<std::size_t, double> GetCorrelationWithInverseParts(QVector<Bar> &x, QVector<Bar> &y, QVector<QVector<int>> splitPoints, CorrelationType type, int priceType)
{
    QList<MappedParameters> mp;

    for(QVector<int> &split : splitPoints)
    {
        mp.push_back(MappedParameters{x, y, split,type, priceType});
    }

    qDebug() << mp.size();

    QFuture<double> results = QtConcurrent::mapped(mp, GetCorrelationWithParameters);

    double maxCorrelation = 0;
    std::size_t idx = 0, i = 0;

    for(QFuture<double>::const_iterator j = results.constBegin(); j != results.constEnd(); ++j )
    {
        if(std::abs((*j)) > maxCorrelation)
        {
            maxCorrelation = std::abs(*j);
            idx = i;
        }

        ++i;
    }

    qDebug() << mp.at(idx).split;

    return QPair<std::size_t, double>{idx, maxCorrelation};
}

QVector<Correlation> GetBestCorrelations(QVector<Bar> &bars, std::size_t firstBar, std::size_t windowSize, int priceType, bool onlyForward)
{
	QVector<Bar> x, y;

	if(!onlyForward)
		firstBar = 0;

	std::size_t n = 0;

	for(std::size_t i = firstBar; i <= bars.size() - 2 * windowSize; ++i)
	{
		for(std::size_t j = i + windowSize; j < bars.size() - windowSize; ++j)
		{
			++n;
		}
	}

	qDebug() << "n is " << n;

	struct Param
	{
		QVector<Bar> * bars;
		std::size_t i, j;
		std::size_t windowSize;
		int priceType;
	};

	const auto & GetAverageCorrelation = [](Param prm)
	{
		QVector<Bar> x = prm.bars->mid(prm.i, prm.windowSize), y = prm.bars->mid(prm.j, prm.windowSize);
		return (GetCorrelation(x, y, kPearson, prm.priceType) + GetCorrelation(x, y, kSpearman, prm.priceType))/2;
	};

	QVector<Param> mp(n);
	auto it = mp.begin();

	QVector<Correlation> result(n);
	auto corItr = result.begin();

	for(std::size_t i = firstBar; i <= bars.size() - 2 * windowSize; ++i)
	{
		for(std::size_t j = i + windowSize; j < bars.size() - windowSize; ++j)
		{
			(*it).priceType = priceType;
			(*it).windowSize = windowSize;
			(*it).bars = &bars;
			(*it).i = i;
			(*it).j = j;
			(*corItr) = Correlation {0, bars.at(i).datetime, bars.at(j).datetime, i, j};

			++it;
			++corItr;
		}
	}

	QVector<double> results = QtConcurrent::blockingMapped(mp, GetAverageCorrelation);
	corItr = result.begin();

	for(QVector<double>::const_iterator j = results.constBegin(); j != results.constEnd(); ++j )
	{
		(*corItr).value = *j;
		++corItr;
	}

	std::sort(std::begin(result), std::end(result), [](const auto & corA, const auto & corB)
	{
		return std::abs(corA.value) > std::abs(corB.value);
	});

	QVector<Correlation> finalResult;
	finalResult.reserve(n);

	for(auto iter = result.begin(); iter < result.end(); ++iter)
	{
		const Correlation & cor = *iter;

		if(std::find_if(std::begin(finalResult), std::end(finalResult), [cor, bars, windowSize](const auto & c)
		{
			if(std::abs(c.value) > std::abs(cor.value))
			{
				if((cor.index1 >= c.index1 && cor.index1 <= c.index1 + windowSize) ||
					(c.index1 >= cor.index1 && c.index1 <= cor.index1 + windowSize))
					{
						if((cor.index2 >= c.index2 && cor.index2 <= c.index2 + windowSize) ||
							(c.index2 >= cor.index2 && c.index2 <= cor.index2 + windowSize))
								return true;
					}
			}
			return false;
		}) == std::end(finalResult))
		{
			finalResult.push_back(*iter);
		}
	}

	for(const auto & correlation : finalResult)
	{
		qDebug() << correlation.dateTime1 << " " << correlation.dateTime2 << " " << correlation.value;
	}

	qDebug() << "result size is " << finalResult.size();

	return finalResult;
}
