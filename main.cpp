#include "ephemeris.h"
#include "mainwindow.h"
#include "version.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QCoreApplication::setOrganizationName("WirBrauchen180");
    QCoreApplication::setOrganizationDomain("wirbrauchen180@gmail.com");
    QCoreApplication::setApplicationName("Fancy Charts");
    QCoreApplication::setApplicationVersion(QString("%1.%2.%3").arg(cVersion, cSubVersion, cPatchVersion));

    MainWindow w;
    w.show();
    return a.exec();
}
