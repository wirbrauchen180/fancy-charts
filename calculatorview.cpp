#include <QDebug>
#include <QWheelEvent>
#include <QTimeLine>

#include "calculatorscene.h"

#include "calculatorview.h"

CalculatorView::CalculatorView(QWidget *parent) : QGraphicsView(parent)
{
    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
}

void CalculatorView::wheelEvent(QWheelEvent *event)
{
    QPoint numPixels = event->pixelDelta();
    QPoint numDegrees = event->angleDelta() / 8;

    if (!numPixels.isNull())
    {
        QPointF numSteps = numPixels / 120.;
        mNumScheduledScalings += numSteps.y();

        if (mNumScheduledScalings * numSteps.y() < 0)
            mNumScheduledScalings = numSteps.y();

    }
    else if (!numDegrees.isNull())
    {
        QPoint numSteps = numDegrees / 15;
        mNumScheduledScalings += numSteps.y();

        if (mNumScheduledScalings * numSteps.y() < 0)
            mNumScheduledScalings = numSteps.y();
    }

    //qDebug() << transform();

    if((transform().m11() < 10 && transform().m11() > 0.1) ||
       (transform().m11() >= 10 && mNumScheduledScalings < 0) ||
       (transform().m11() <= 0.1 && mNumScheduledScalings > 0))
    {
        QTimeLine *anim = new QTimeLine(360, this);
        anim->setUpdateInterval(20);

        connect(anim, &QTimeLine::valueChanged, this, &CalculatorView::scalingTime);
        connect(anim, &QTimeLine::finished, this, &CalculatorView::animFinished);
        anim->start();
    }
    else
        mNumScheduledScalings = 0;

    event->accept();
}

void CalculatorView::scalingTime(qreal /*x*/)
{
    const ViewportAnchor anchor = transformationAnchor();
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

    qreal factor = 1.0 + mNumScheduledScalings / 360.0;

    if(transform().m11() * factor > 10)
        factor = 10. / transform().m11();
    else if (transform().m11() * factor < 0.1)
        factor = 0.1 / transform().m11();

    scale(factor, factor);

    CalculatorScene *sq = qobject_cast<CalculatorScene*>(scene());
    if(sq)
        sq->clearOverlays(viewportTransform());

    setTransformationAnchor(anchor);
}

void CalculatorView::animFinished()
{
    if (mNumScheduledScalings > 0)
        mNumScheduledScalings -= 1;
    else
        mNumScheduledScalings += 1;

    sender()->deleteLater();
}
