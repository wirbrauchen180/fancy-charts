#include "chartwindow.h"
#include "colorselectwindow.h"
#include "cpfinancial.h"

#include "ui_chartwindow.h"

ChartWindow::ChartWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ChartWindow)
{
    ui->setupUi(this);

    ui->customPlot->setInteractions(QCP::iSelectPlottables);

    ui->customPlot->axisRect(0)->setRangeDrag(Qt::Horizontal);
    ui->customPlot->setInteraction(QCP::iRangeDrag, true);
    ui->customPlot->axisRect(0)->setRangeZoom(Qt::Horizontal);
    ui->customPlot->setInteraction(QCP::iRangeZoom, true);

    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlot, QOverload<const QPoint&>::of(&ChartWindow::customContextMenuRequested), this, QOverload<const QPoint&>::of(&ChartWindow::contextMenuRequest));    

    QPixmap pm(1920, 1080);

    /*QRadialGradient rg(pm.rect().center(), pm.rect().height()/2);
    rg.setColorAt(0, QColor(255, 233, 255));
    rg.setColorAt(1, QColor(233, 255, 255));
    rg.setSpread(QGradient::RepeatSpread);*/

    QLinearGradient lg(pm.rect().topLeft(), pm.rect().bottomRight());
    lg.setColorAt(0, QColor(255, 233, 255));
    lg.setColorAt(1, QColor(233, 255, 255));
    {
        QPainter painter(&pm);
        painter.fillRect(pm.rect(), lg);
    }

    ui->customPlot->setBackground(pm);
    ui->customPlot->setBackgroundScaled(true);

    ui->customPlot->replot();
}

ChartWindow::~ChartWindow()
{
    delete ui;
}

void ChartWindow::drawChart(QString title, QVector<Bar> bars)
{
    new CPFinancial(title, bars, ui->customPlot->xAxis, ui->customPlot->yAxis);
    ui->customPlot->replot();
}

void ChartWindow::save(QDataStream &stream)
{
    for( int i = 0; i < ui->customPlot->plottableCount(); ++i )
    {
        CPFinancial *cp = qobject_cast<CPFinancial *>(ui->customPlot->plottable(i));

        if(cp)
        {
            stream << (quint32)ClassType::CPFinancialType;
            stream << *cp;
        }
    }

    for( int i = 0; i < ui->customPlot->graphCount(); ++i )
    {
        /*CPFinancial *cp = qobject_cast<CPFinancial *>(ui->customPlot->plottable(i));

        if(cp)
        {
            stream << (quint32)ClassType::CPFinancialType;
            stream << *cp;
        }*/
    }
}

void ChartWindow::parse(QString title, QDataStream &stream)
{
    while(!stream.atEnd())
    {
        ClassType ct;
        quint32 type;

        stream >> type;
        ct = (ClassType)type;

        switch(ct)
        {
            case ClassType::CPFinancialType:
            {
                QVector<Bar> bars;
                NamesAndColors colors;

                stream >> bars >> colors;

                new CPFinancial(title, bars, ui->customPlot->xAxis, ui->customPlot->yAxis, colors);
            }
            break;

        default:

            break;
        }
    }

    ui->customPlot->replot();
}

void ChartWindow::contextMenuRequest(const QPoint &pos)
{
    if (!ui->customPlot->selectedPlottables().empty())
    {
      QMenu *menu = new QMenu(this);
      menu->setAttribute(Qt::WA_DeleteOnClose);

      menu->addAction("Properties", this, &ChartWindow::setProperties);
      menu->popup(ui->customPlot->mapToGlobal(pos));
    }
}

void ChartWindow::setProperties()
{
    if (ui->customPlot->selectedPlottables().size() == 1)
    {
        CPFinancial *ohlc = qobject_cast<CPFinancial*>(ui->customPlot->selectedPlottables().at(0));

        if(ohlc)
        {
            ohlc->setSelection(QCPDataSelection{});
            ui->customPlot->replot();

            ColorSelectWindow *pw = new ColorSelectWindow(ohlc->getColorsData(), qobject_cast<QWidget*>(parent()));
            pw->setAttribute(Qt::WA_DeleteOnClose);
            connect(pw, &ColorSelectWindow::repaintParent, ohlc, &CPFinancial::repaint);
            pw->show();
        }
    }
}

const QString &ChartWindow::getFile() const
{
    return file;
}

void ChartWindow::setFile(const QString &newFile)
{
    file = newFile;
}
