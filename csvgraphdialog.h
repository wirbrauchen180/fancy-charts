#ifndef CSVGRAPHDIALOG_H
#define CSVGRAPHDIALOG_H

#include "bar.h"

#include <QDialog>

namespace Ui {
class CSVGraphDialog;
}

class CSVGraphDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CSVGraphDialog(QWidget *parent = nullptr);
    ~CSVGraphDialog();

    QVector<Bar> getBars() const;
    QString getTitle() const;

private slots:
    void chooseFile();
    void parse();
    void accepted();
    void updateProgress(int);
    void fileProgress(int);

    void on_parseFinished();

private:
    Ui::CSVGraphDialog *ui;

    QVector<Bar> mBars;
};

#endif // CSVGRAPHDIALOG_H
