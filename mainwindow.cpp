#include <QDebug>
#include <QMdiSubWindow>

#include "csvparser.h"
#include "csvgraphdialog.h"

#include "chartwindow.h"
#include "mainwindow.h"
#include "calculatorwindow.h"
#include "ui_mainwindow.h"

#include "correlationoverlaygraph.h"

#include "version.h"

const quint32 saveMagic = 0x1D1C1B1A;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->actionAddChart, &QAction::triggered, this, &MainWindow::addChart);
	connect(ui->actionSaveAs, &QAction::triggered, this, [this](){this->saveAs();} );
	connect(ui->actionSave, &QAction::triggered, this, [this](){this->saveAs(true);});
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::exitWindow);
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::open);
    connect(ui->actionSquareOf9, &QAction::triggered, this, &MainWindow::addSquareOf9);
	connect(ui->actionCorrelationGraph, &QAction::triggered, this, &MainWindow::addCorrelationGraph);

	centralWidget()->layout()->setContentsMargins(0, 0, 0, 0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addChart()
{
    CSVGraphDialog dlg(this);
    if(dlg.exec() == QDialog::Accepted)
    {
        ChartWindow *cp = new ChartWindow(this);
        createSubwindow(cp, dlg.getTitle());

        cp->drawChart(dlg.getTitle(), dlg.getBars());
    }
}


void MainWindow::exitWindow()
{
    QCoreApplication::quit();
}

void MainWindow::saveAs(bool useCurrentFile)
{
    QMdiSubWindow *sw = ui->mdiArea->activeSubWindow();
    ChartWindow *cp = qobject_cast<ChartWindow*>(sw->widget());

    QString fileName = useCurrentFile ? cp->getFile() : QString{};

    if(cp)
    {
        if(!fileName.size())
            fileName = QFileDialog::getSaveFileName(this, QString("Save %1 as: ").arg(sw->windowTitle()),
                                                        QDir::currentPath().append(QDir::separator()).append(sw->windowTitle()), "*.gwc");

        if(fileName.size())
        {
            QFile inputFile(fileName);
            if (inputFile.open(QIODevice::WriteOnly))
            {
                QDataStream stream(&inputFile);

                stream << (quint32)saveMagic;
                stream << (qint32)saveVersion;
                stream << (qint32)saveSubVersion;

                stream.setVersion(QDataStream::Qt_5_15);

                cp->save(stream);
                cp->setFile(fileName);
            }
            else
            {
                QMessageBox msgBox;
                msgBox.setText(inputFile.errorString());
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }

            inputFile.close();
        }
    }
}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open File"), QDir::currentPath(), "*.gwc");
    QString error = "";

    if(fileName.size())
    {
        QFile inputFile(fileName);
        if (inputFile.open(QIODevice::ReadOnly))
        {
            QDataStream stream(&inputFile);

            quint32 magic, version, subVersion;

            stream >> magic;
            stream >> version;
            stream >> subVersion;

            if(magic != saveMagic)
                error = "Bad file format";
            else if(version != saveVersion || saveSubVersion != subVersion)
                error = "File format is incompatible with current version";
            else
            {
                ChartWindow *cp = new ChartWindow(this);                

                QString onlyFileName = fileName.section("/",-1,-1);
                QString title = onlyFileName.section("/",-1,-1).mid(0, onlyFileName.lastIndexOf("."));

                cp->parse(title, stream);
                cp->setFile(fileName);

                createSubwindow(cp, title);
            }

            if(error.size())
            {
                QMessageBox msgBox;
                msgBox.setText(error);
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.exec();
            }
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText(inputFile.errorString());
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }

        inputFile.close();
    }

}

void MainWindow::addSquareOf9()
{
    CalculatorWindow *cw = new CalculatorWindow(this);
    createSubwindow(cw, "Square of 9");
}

void MainWindow::addCorrelationGraph()
{
	CorrelationOverlayGraph *cw = new CorrelationOverlayGraph(this);
	createSubwindow(cw, "Correlation Graph");
}

QMdiSubWindow *MainWindow::createSubwindow(QWidget *widget, QString title)
{
    QMdiSubWindow *chart = ui->mdiArea->addSubWindow(widget);

    chart->setWindowTitle(title);
    chart->setWindowIcon(QIcon(":/FancyCharts.ico"));
    chart->show();

    return chart;
}
