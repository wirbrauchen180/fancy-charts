#ifndef CSVPARSER_H
#define CSVPARSER_H

#include "bar.h"

#include <QBitArray>
#include <QObject>
#include <QString>
#include <QVector>

enum DataOrder
{
    Open,
    High,
    Low,
    Close,
    Volume
};

class CSVParser : public QObject
{
    Q_OBJECT

public:
    CSVParser(QString format);

    void parse(QString fileNames, bool ignoreFirstLine = true);

    bool checkErrors();
    QString errorsString();

    QVector<Bar> getBars() const
    {
        return bars;
    }

signals:
    void progressChanged(int value);
    void fileChanged(int value);
    void parseFinished();

private:    
    QVector<QString> errors;

    QVector<DataOrder> order;
    QString datetimeFormat;

    QVector<Bar> bars;
};

#endif // CSVPARSER_H
