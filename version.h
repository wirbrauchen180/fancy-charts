#ifndef VERSION_H
#define VERSION_H

#include <qmath.h>

const quint32 saveVersion = 0;
const quint32 saveSubVersion = 1;

const quint32 cVersion = 0;
const quint32 cSubVersion = 1;
const quint32 cPatchVersion = 0;

#endif // VERSION_H
