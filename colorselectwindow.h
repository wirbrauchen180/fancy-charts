#ifndef COLORSELECTWINDOW_H
#define COLORSELECTWINDOW_H

#include <QColor>
#include <QDialog>

namespace Ui {
class ColorSelectWindow;
}

typedef QVector<QPair<QString, QColor>> NamesAndColors;

class ColorSelectWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ColorSelectWindow(NamesAndColors &colors, QWidget *parent = nullptr);
    ~ColorSelectWindow();

private slots:
    void changeColor();

signals:
    void repaintParent();

private:
    Ui::ColorSelectWindow *ui;
    NamesAndColors &colorsData;

    QVector<QColor> colors;
};

#endif // COLORSELECTWINDOW_H
