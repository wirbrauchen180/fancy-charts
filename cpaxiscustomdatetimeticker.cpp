#include "cpaxiscustomdatetimeticker.h"

#include "common.h"

CPAxisCustomDateTimeTicker::CPAxisCustomDateTimeTicker(const QVector<QDateTime> &bars) : QCPAxisTicker(), mBars(bars)
{

}

void CPAxisCustomDateTimeTicker::generate(const QCPRange &range, const QLocale&, QChar, int, QVector<double> &ticks, QVector<double> *, QVector<QString> *tickLabels)
{
    ticks = createTickVector(getTickStep(range), existingRange(range));

    if(!ticks.empty())
    {
        tickLabels->clear();

        int i = 0;
        for(; i < ticks.size(); ++i)
        {
            int idx = int(std::floor(ticks.at(i)));

            if(idx >= 0 && idx < mBars.size())
                tickLabels->push_back(createLabel(idx));
			else
				tickLabels->push_back("");
        }

        cleanTicks(getTickStep(range), ticks, *tickLabels);
    }
}

QString CPAxisCustomDateTimeTicker::createLabel(int index, bool firstOrLast)
{
    TimeFrame tf = calculateTimeFrame();
    QString format, label;

    switch(tf)
    {
        case tfMinute:
            format = ":mm";
        break;

        case tfHour:
            format = "hh";
        break;

        case tfDay:
            format = "dd";
        break;

        case tfWeek:
            format = "dd.MM";
        break;

        case tfMonth:
            format = "MMM";
        break;

        case tfYear:
            format = "yyyy";
        break;

        default:
            format = "hh:mm dd.MM.yyyy";
        break;
    }

    QString extendedFormat = format;

    if(!index || firstOrLast)
    {
        switch(tf)
        {
            case tfMinute:
            case tfHour:
                extendedFormat = "hh:mm\ndd.MM.yyyy";
            break;

            case tfDay:
            case tfWeek:
                extendedFormat = "dd.MM.yyyy";
            break;

            case tfMonth:
                extendedFormat = "MMM yyyy";
            break;

            default:
            break;
        }

		label = mBars.at(index).toString(extendedFormat);
    }
    else if(getTickPriority(index) == prYear)
    {
        switch(tf)
        {
            case tfMinute:
            case tfHour:
                extendedFormat = "hh:mm\ndd.MM.yyyy";
            break;

            case tfDay:
            case tfWeek:
                extendedFormat = "dd.MM.yyyy";
            break;

            case tfMonth:
                extendedFormat = "yyyy";
            break;

            default:
            break;
        }

		label = mBars.at(index).toString(extendedFormat);
    }
    else if( getTickPriority(index) == prMonth)
    {
        switch(tf)
        {
            case tfMinute:
            case tfHour:
                extendedFormat = "hh:mm\ndd.MM.yyyy";
            break;

            case tfDay:
            case tfWeek:
                extendedFormat = "dd.MM";
            break;

            case tfMonth:
                extendedFormat = "MMM";
            break;

            default:
            break;
        }

		label = mBars.at(index).toString(extendedFormat);
    }
    else if( getTickPriority(index) == prDay)
    {
        switch(tf)
        {
            case tfMinute:
            case tfHour:
                extendedFormat = "hh:mm\ndd.MM.yyyy";
            break;

            case tfWeek:
                extendedFormat = "dd";
            break;

            default:
            break;
        }

		label = mBars.at(index).toString(extendedFormat);
    }
    else if( getTickPriority(index) == prHour)
    {
        switch(tf)
        {
            case tfMinute:
                extendedFormat = "hh:mm";
            break;

            default:
            break;
        }

		label = mBars.at(index).toString(extendedFormat);
    }
    else
		label = mBars.at(index).toString(format);

    return label;

}

void CPAxisCustomDateTimeTicker::cleanTicks(double step, QVector<double> &ticks, QVector<QString> &labels)
{
    bool clean = false;

    while(!clean)
    {
        clean = true;

        for(int i = 0; i < ticks.size() - 1; ++i)
        {
            if(std::abs(ticks.at(i) - ticks.at(i+1)) < step)
            {
                if(!i)
                {
                    ticks.removeAt(1);
                    labels.removeAt(1);
                }
                else if((getTickPriority(int(std::round(ticks.at(i)))) < getTickPriority(int(std::round(ticks.at(i+1))))) || (i == ticks.size() - 2))
                {
                    ticks.removeAt(i);
                    labels.removeAt(i);
                }
                else
                {
                    ticks.removeAt(i+1);
                    labels.removeAt(i+1);
                }

                clean = false;
                break;
            }
        }
    }
}

QCPRange CPAxisCustomDateTimeTicker::existingRange(const QCPRange &range)
{
    QCPRange result = range;

    if(result.upper >= mBars.size())
        result.upper = mBars.size() - 1;

    return result;
}

double CPAxisCustomDateTimeTicker::getTickStep(const QCPRange &range)
{
    return QCPAxisTicker::getTickStep(range)/2;
}

QVector<double> CPAxisCustomDateTimeTicker::createTickVector(double, const QCPRange &range)
{
    QVector<double> ticks;

    int lower = std::ceil(range.lower);
    int upper = std::floor(range.upper);

    for(int i = lower; i <= upper; ++i)
        ticks.push_back(i);

    return ticks;
}

TimeFrame CPAxisCustomDateTimeTicker::calculateTimeFrame()
{
	int secondsTF = qMin(mBars.at(0).secsTo(mBars.at(1)), mBars.at(1).secsTo(mBars.at(2)));

    TimeFrame tf = tfUnknown;

    switch(secondsTF)
    {
        case 60:
            tf = tfMinute;
            break;
        case 3600:
            tf = tfHour;
            break;
        case 86400:
            tf = tfDay;
            break;
        case 604800:
            tf = tfWeek;
            break;
        default:
            {
				if(mBars.at(0).date().month() != mBars.at(1).date().month() &&
						mBars.at(1).date().month() != mBars.at(2).date().month())
                    tf = tfMonth;
				else if(mBars.at(0).date().year() != mBars.at(1).date().year() &&
						mBars.at(1).date().year() != mBars.at(2).date().year())
                    tf = tfYear;
            }
            break;
    }

    return tf;
}

Priority CPAxisCustomDateTimeTicker::getTickPriority(int index)
{
    Priority pr = prDefault;

	if(index > 0 && mBars.at(index - 1).date().year() != mBars.at(index).date().year())
        pr = prYear;
	else if(index > 0 && mBars.at(index - 1).date().month() != mBars.at(index).date().month())
        pr = prMonth;
	else if(index > 0 && mBars.at(index - 1).date().day() != mBars.at(index).date().day())
        pr = prDay;
	else if(index > 0 && mBars.at(index - 1).time().hour() != mBars.at(index).time().hour())
        pr = prHour;

    return pr;
}
