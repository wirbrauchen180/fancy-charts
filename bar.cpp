#include <QDataStream>

#include "bar.h"

QDataStream &operator<<(QDataStream &dataStream, const Bar bar)
{
    dataStream << bar.datetime << bar.Open << bar.High << bar.Low << bar.Close << bar.Volume;
    return dataStream;
}

QDataStream &operator>>(QDataStream &dataStream, Bar bar)
{
    dataStream >> bar.datetime >> bar.Open >> bar.High >> bar.Low >> bar.Close >> bar.Volume;
    return dataStream;
}

QDataStream &operator<<(QDataStream &dataStream, const Bar &bar)
{
    dataStream << bar.datetime << bar.Open << bar.High << bar.Low << bar.Close << bar.Volume;
    return dataStream;
}

QDataStream &operator>>(QDataStream &dataStream, Bar &bar)
{
    dataStream >> bar.datetime >> bar.Open >> bar.High >> bar.Low >> bar.Close >> bar.Volume;
    return dataStream;
}
