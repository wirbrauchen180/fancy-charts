#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QRegularExpression>

#include "csvgraphdialog.h"
#include "csvparser.h"
#include "ui_csvgraphdialog.h"
#include "mainwindow.h"

CSVGraphDialog::CSVGraphDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CSVGraphDialog)
{
    ui->setupUi(this);

    connect(ui->chooseFileButton, &QPushButton::clicked, this, &CSVGraphDialog::chooseFile);
    connect(ui->parsePushButton, &QPushButton::clicked, this, &CSVGraphDialog::parse);
    connect(ui->buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &CSVGraphDialog::accept);
}

CSVGraphDialog::~CSVGraphDialog()
{
    delete ui;
}

QVector<Bar> CSVGraphDialog::getBars() const
{
    return mBars;
}

QString CSVGraphDialog::getTitle() const
{
    QString title = ui->titleLineEdit->text();

    if(!title.size())
        title = "Untitled chart";

    return title;
}

QString LongestCommonSubstring(QString const& qa, QString const& qb)
{
    std::string a = qa.toStdString(), b = qb.toStdString();

    std::vector<std::string> substrings;
    for (auto beg = a.begin(); beg != std::prev(a.end()); ++beg)
        for (auto end = beg; end != a.end(); ++end)
            if (b.find(std::string(beg, std::next(end))) != std::string::npos)
                substrings.emplace_back(beg, std::next(end));
    return QString::fromStdString(*std::max_element(substrings.begin(), substrings.end(),
           [](auto& elem1, auto& elem2) { return elem1.length() < elem2.length(); }));
}

void CSVGraphDialog::chooseFile()
{
    QStringList fileNames = QFileDialog::getOpenFileNames(this);

    ui->parsePushButton->setEnabled(false);
    ui->FilePreviewTextEdit->clear();
    ui->fileNameLineEdit->clear();
    ui->titleLineEdit->clear();

    if(!fileNames.empty())
        ui->fileNameLineEdit->setText(fileNames.join(";"));

    QString commonFileName;

    for(QString &fileName : fileNames)
    {
        if(fileName.size())
        {
            if(!commonFileName.size())
                commonFileName = fileName;
            else
                commonFileName =  LongestCommonSubstring(commonFileName, fileName);

            QFile inputFile(fileName);
            if (inputFile.open(QIODevice::ReadOnly))
            {
                ui->FilePreviewTextEdit->append(fileName);

                QTextStream in(&inputFile);

                for(int i = 0; i < 25 && !in.atEnd(); ++i)
                {
                    ui->FilePreviewTextEdit->append(in.readLine());
                }

                ui->parsePushButton->setEnabled(true);
            }
        }
    }

    if(fileNames.size() > 1)
        ui->filesProgressBar->setMaximum(fileNames.size());

    if(commonFileName.size())
    {
        QString onlyFileName = commonFileName.section("/",-1,-1);
        ui->titleLineEdit->setText(onlyFileName.section("/",-1,-1).mid(0, onlyFileName.lastIndexOf(".")));

		int idx = onlyFileName.lastIndexOf(QRegularExpression("[a-zA-Z0-9]"));
        if(idx != -1)
            onlyFileName = onlyFileName.mid(0, idx + 1);

        ui->titleLineEdit->setText(onlyFileName);
    }
}


void CSVGraphDialog::parse()
{
    ui->buttonBox->setEnabled(false);
    ui->ParseProgressBar->setValue(0);

    CSVParser *p = new CSVParser(ui->formatLineEdit->text());

    QObject::connect(p, &CSVParser::progressChanged, this, &CSVGraphDialog::updateProgress);
    QObject::connect(p, &CSVParser::fileChanged, this, &CSVGraphDialog::fileProgress);
    QObject::connect(p, &CSVParser::parseFinished, this, &CSVGraphDialog::on_parseFinished);

    if(p->checkErrors())
    {
        QMessageBox msgBox;
        msgBox.setText(p->errorsString());
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.exec();
    }
    else
    {
        p->parse(ui->fileNameLineEdit->text(), ui->skipFirstLineCheckBox->isChecked());
    }

}


void CSVGraphDialog::accepted()
{

}

void CSVGraphDialog::updateProgress(int value)
{
    ui->ParseProgressBar->setValue(value);
    QApplication::processEvents();
}

void CSVGraphDialog::fileProgress(int value)
{
    ui->filesProgressBar->setValue(value);
    QApplication::processEvents();
}

void CSVGraphDialog::on_parseFinished()
{
    CSVParser *s = qobject_cast<CSVParser*>(sender());
    if(s)
    {
        if(s->checkErrors())
        {
            QMessageBox msgBox;
            msgBox.setText(s->errorsString());
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }
        else
        {
            mBars = s->getBars();            
            ui->buttonBox->setEnabled(true);
        }

        s->deleteLater();
    }
}

