#ifndef COMMON_H
#define COMMON_H

#include <QDateTime>

template<typename T>
QVector<QDateTime> toDateTime(const QVector<T> &data)
{
	QVector<QDateTime> result;

	std::transform(data.begin(), data.end(), std::back_inserter(result), [](T t){return t.datetime;});

	return result;
}

enum GraphicsItemType
{
    kLabel = 1,
    kCeil
};

#endif // COMMON_H
