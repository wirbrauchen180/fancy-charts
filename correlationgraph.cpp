#include "correlationgraph.h"
#include "ui_correlationgraph.h"
#include "qcustomplot.h"
#include "cpaxiscustomdatetimeticker.h"
#include "common.h"

CorrelationGraph::CorrelationGraph(QVector<Correlation> data_, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CorrelationGraph), data(data_)
{
    ui->setupUi(this);

    QCPBars *correlationBarsPositive = new QCPBars(ui->correlationGraph->xAxis, ui->correlationGraph->yAxis);
    QCPBars *correlationBarsNegative = new QCPBars(ui->correlationGraph->xAxis, ui->correlationGraph->yAxis);

    int idx = 0;

	QVector<QDateTime> date1;
	QVector<QDateTime> date2;

	std::transform(data.begin(), data.end(), std::back_inserter(date1), [](auto t){return t.dateTime1;});
	std::transform(data.begin(), data.end(), std::back_inserter(date2), [](auto t){return t.dateTime2;});

	QSharedPointer<CPAxisCustomDateTimeTicker> tickerText(new CPAxisCustomDateTimeTicker(date1));
	QSharedPointer<CPAxisCustomDateTimeTicker> tickerText2(new CPAxisCustomDateTimeTicker(date2));

    for(Correlation corr : data)
    {
        if(corr.value > 0)
            correlationBarsPositive->addData(idx, corr.value);
        else if(corr.value < 0)
            correlationBarsNegative->addData(idx, corr.value);

		// tickerText->addTick(static_cast<double>(idx), corr.dateTime1.toString("dd.MM.yyyy"));
		// tickerText2->addTick(static_cast<double>(idx), corr.dateTime2.toString("dd.MM.yyyy"));
        ++idx;
    }

    ui->correlationGraph->xAxis->setTicker(tickerText);

    correlationBarsPositive->setWidthType(QCPBars::WidthType::wtPlotCoords);
    correlationBarsPositive->setWidth(0.5);
    correlationBarsPositive->setPen(Qt::NoPen);
    correlationBarsPositive->setBrush(QColor(100, 180, 110));

    correlationBarsNegative->setWidthType(QCPBars::WidthType::wtPlotCoords);
    correlationBarsNegative->setWidth(0.5);
    correlationBarsNegative->setPen(Qt::NoPen);
    correlationBarsNegative->setBrush(QColor(180, 90, 90));

    ui->correlationGraph->xAxis->rescale();
    ui->correlationGraph->yAxis->rescale();

    ui->correlationGraph->xAxis->setRange(data.size() - data.size()/10 - 1, data.size() - 1);
    ui->correlationGraph->yAxis->setRange(-1.1, 1.1);
    ui->correlationGraph->yAxis->ticker()->setTickCount(9);

    ui->correlationGraph->xAxis2->setTicker(tickerText2);
    ui->correlationGraph->xAxis2->rescale();
    ui->correlationGraph->xAxis2->setRange(data.size() - data.size()/10 - 1, data.size() - 1);
    ui->correlationGraph->xAxis2->scaleRange(1.025, ui->correlationGraph->xAxis->range().center());
    ui->correlationGraph->xAxis2->setVisible(true);

    ui->correlationGraph->axisRect(0)->setRangeDrag(Qt::Horizontal);
    ui->correlationGraph->setInteraction(QCP::iRangeDrag, true);
    ui->correlationGraph->axisRect(0)->setRangeZoom(Qt::Horizontal);
    ui->correlationGraph->setInteraction(QCP::iRangeZoom, true);

    connect(ui->correlationGraph->xAxis, QOverload<const QCPRange&>::of(&QCPAxis::rangeChanged), this, [this](const QCPRange &newRange)
    {
        if(newRange.lower < -1)
        {
            this->ui->correlationGraph->xAxis->setRange(QCPRange(0, newRange.upper));
        }
        else if(newRange.upper > this->data.size() + 1)
        {
            this->ui->correlationGraph->xAxis->setRange(QCPRange(newRange.lower, this->data.size()));
        }

        this->ui->correlationGraph->xAxis2->setRange(this->ui->correlationGraph->xAxis->range());
    });

    ui->correlationGraph->replot();
}

CorrelationGraph::~CorrelationGraph()
{
    delete ui;
}
