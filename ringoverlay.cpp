#include <cmath>

#include <qmath.h>
#include <QDate>
#include <QDebug>
#include <QPen>
#include <QGraphicsLineItem>
#include <QGraphicsTextItem>

#include "arrow.h"
#include "label.h"
#include "ringoverlay.h"

std::vector<int> primeFactors(int n)
{
    std::vector<int> result;
    while (n % 2 == 0)
    {
        result.push_back(2);
        n = n/2;
    }

    for (int i = 3; i <= std::sqrt(n); i = i + 2)
    {
        while (n % i == 0)
        {
            result.push_back(i);
            n = n/i;
        }
    }

    if (n > 2)
        result.push_back(n);

    return result;
}

int commonFactorsSize(int number)
{
    std::vector<int> v1 = primeFactors(number),
                     v2 = primeFactors(360);
    std::vector<int> v_intersection;

    std::set_intersection(v1.begin(), v1.end(),
                          v2.begin(), v2.end(),
                          std::back_inserter(v_intersection));

    return v_intersection.size();
}

int datePriority(QDate date)
{
    if(date.day() == 21 && !(date.month() % 3))
        return 5;

    if(date.day() == 1)
        return 4;

    if(date.day() == 15)
        return 3;

    if(date.day() == 10 || date.day() == 20)
        return 2;

    if(date.day() == 5 || date.day() == 25)
        return 1;

    return 0;
}

RingOverlay::RingOverlay(OverlayType type, qreal radius, qreal shift, int numOfArrows, bool connectArrows, qreal startAngle, qreal angleSpan, QGraphicsItem *parent)
    : QGraphicsItemGroup(), mRadius(radius), mShift(shift), mType(type)
{
    mEllipse = new QGraphicsEllipseItem(0 - mRadius - mShift, 0 - mRadius - mShift, 2 * (mRadius + mShift), 2 * (mRadius + mShift), parent);
    mEllipse->setPen(QPen(Qt::gray, 2));

    addToGroup(mEllipse);

    qreal angle = 180 - startAngle;

    for(int i = 0; i < numOfArrows; ++i)
    {
        QLineF line(QPointF(0, 0), QPointF(- mRadius - mShift, 0));
        line.setAngle(angle);

        if(connectArrows)
        {
            QLineF line2(QPointF(0, 0), QPointF(- mRadius - mShift, 0));
            line2.setAngle(angle - angleSpan/numOfArrows);

            auto ln = new QGraphicsLineItem(line.p2().x(), line.p2().y(), line2.p2().x(), line2.p2().y());
            ln->setPen(QPen(QBrush(QColor(254, 111, 94)), 1, Qt::DashDotLine));
            addToGroup(ln);

            mConnectors.push_back(ln);
        }

        auto arrow = new Arrow(0, 0, line.p2().x(), line.p2().y());
        addToGroup(arrow);
        mArrows.push_back(arrow);

        angle -= angleSpan/numOfArrows;
    }

    createTicks();
}

void RingOverlay::clearLabels(QTransform t)
{
    adjustLabelsPos(t);

    for(int i = 0; i < mTicks.size(); ++i)
        mTicks.at(i).second->show();

    for(int i = 0; i < mTicks.size(); ++i)
    {
        if(!mTicks.at(i).second->isVisible())
            continue;

        for(int j = i + 1; j < mTicks.size(); ++j)
        {
            if(!mTicks.at(j).second->isVisible())
                continue;

            if(mTicks.at(i).second->collidesWithItemTransform(mTicks.at(j).second, t))
            {
                if(mType == OverlayType::kDegrees)
                {
                    if(commonFactorsSize(i > 0 ? i : 360) >= commonFactorsSize(j))
                        mTicks.at(j).second->hide();
                    else
                        mTicks.at(i).second->hide();
                }
                else if(mType == OverlayType::kDays)
                {
                    if(datePriority(mTicks.at(i).second->getValue().toDate())
                            > datePriority(mTicks.at(j).second->getValue().toDate()))
                        mTicks.at(j).second->hide();
                    else
                        mTicks.at(i).second->hide();
                }
            }
        }
    }
}

void RingOverlay::adjustLabelsPos(QTransform t)
{
    if(!t.isIdentity())
    {
        QRectF rect = mEllipse->rect();
        QPointF center = rect.center();
        rect.setHeight(2*mRadius + 2*mShift / t.m11());
        rect.setWidth(2*mRadius + 2*mShift / t.m11());
        rect.moveCenter(center);
        mEllipse->setRect(rect);

        for(int i = 0; i < mTicks.size(); ++i)
        {
            QLineF line{{0,0}, {-mRadius - mShift / t.m11() - mTicks.at(i).first->line().length()/2 - mTicks.at(i).second->getShift() / t.m11(), 0}};

            line.setAngle(mTicks.at(i).first->line().angle());
            mTicks.at(i).second->setPos(line.p2());

            line.setLength(mRadius + mShift / t.m11() + mTicks.at(i).first->line().length()/2);
            QLineF line2{line.p2(), line.p1()};
            line2.setLength(mTicks.at(i).first->line().length());

            line.setLength(mRadius + mShift + mTicks.at(i).first->line().length()/2);
            mTicks.at(i).first->setPos(-line2.p1() + line.p2());
        }

        for(int i = 0 ; i < mArrows.size(); ++i)
        {
            mArrows.at(i)->setLength(mRadius + mShift / t.m11());

            int nextIdx = (i == mArrows.size() - 1 ? 0 : i + 1);

            mConnectors.at(i)->setLine(QLineF(mArrows.at(i)->endPoint(), mArrows.at(nextIdx)->endPoint()));
        }
    }
}

void RingOverlay::createTicks()
{
    if(mType == OverlayType::kDegrees)
    {
        for(int j = 0; j < 360; ++j)
        {
            int common = commonFactorsSize(j > 0 ? j : 360);

            qreal tickLength = 5 + mRadius * common / 80.;

            QLineF line(QPointF(0, 0), QPointF(-mRadius - mShift - tickLength/2., 0));
            line.setAngle(180 - j);

            QLineF line1(line.p2(), line.p1());
            line1.setLength(tickLength);

            auto tick = new QGraphicsLineItem(line1);
            tick->setPen(QPen(Qt::black, 0.05 + common / 12.));

            auto text = new Label{line.p2(), 25, 40, 15, QString::number(j).append(QString::fromUtf8("\u00B0"))};
            mTicks.push_back(LabeledTick(tick, text));

            addToGroup(tick);
            addToGroup(text);
        }
    }
    else if(mType == OverlayType::kDays)
    {
        QDate dt1 = QDate::fromString("21.03.2022", "dd.MM.yyyy");
        QDate dt2 = QDate::fromString("21.06.2022", "dd.MM.yyyy");
        QDate dt3 = QDate::fromString("21.09.2022", "dd.MM.yyyy");
        QDate dt4 = QDate::fromString("21.12.2022", "dd.MM.yyyy");

        QDate dtStart = dt1;
        QDate dtEnd = dt2;
        int angle = 180;

        do
        {
            int ticks = dtStart.daysTo(dtEnd);

            for(int i = 0; i < ticks; ++i)
            {
                int common = datePriority(dtStart.addDays(i));

                qreal tickLength = 5 + mRadius * common / 140.;

                QLineF line(QPointF(0, 0), QPointF(-mRadius - mShift - tickLength/2., 0));
                line.setAngle(angle - 90 * (double(i)/ticks));

                QLineF line1(line.p2(), line.p1());
                line1.setLength(tickLength);

                auto tick = new QGraphicsLineItem(line1);
                tick->setPen(QPen(Qt::black, 0.05 + common / 12.));

                auto text = new Label{line.p2(), 35, 50, 15, dtStart.addDays(i)};
                mTicks.push_back(LabeledTick(tick, text));

                addToGroup(tick);
                addToGroup(text);
            }

            angle -= 90;

            if(dtEnd == dt2)
            {
                dtEnd = dt3;
                dtStart = dt2;                
            }
            else if(dtEnd == dt3)
            {
                dtEnd = dt4;
                dtStart = dt3;
            }
            else if(dtEnd == dt4)
            {
                dtEnd = dt1.addYears(1);
                dtStart = dt4;
            }
            else
                dtStart = dtEnd;
        }
        while(dtStart != dtEnd);
    }
}
