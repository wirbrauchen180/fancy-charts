#ifndef EPHEMERIS_H
#define EPHEMERIS_H

#include <QDateTime>
#include <QMap>

#include "sweph/swedll.h"

enum DataType
{
    kLongitude = 0,
    kLatitude,
    kDistance,
    kSpeedLong,
    kSpeedLat,
    kSpeedDist,
    kSize
};

typedef QHash<QDateTime, QVector<double>> PlanetData;

class Planet
{
public:
    explicit Planet(int planetIndex);

    double Latitude(QDateTime dt, bool helio);
    double Longitude(QDateTime dt, bool helio);

    double speedLong(QDateTime dt, bool helio);

    QDateTime At(double longitude, QDateTime after, bool helio = false);

private:
    bool calc(QDateTime dt, bool helio);

    int mPlanetIndex;
    PlanetData mDataGeo;
    PlanetData mDataHelio;
};

class Ephemeris
{
private:
    Ephemeris();
    ~Ephemeris();

public:
    void test();
    static Ephemeris *getInstance();

    Ephemeris(const Ephemeris&) = delete;
    Ephemeris& operator=(const Ephemeris&) = delete;

    void addErrors(QString error);
    void clearErrors();
    QStringList getErrors();

    Planet *Sun(){return &mSun;}
    Planet *Mercury(){return &mMercury;}
    Planet *Venus(){return &mVenus;}
    Planet *Moon(){return &mMoon;}
    Planet *Mars(){return &mMars;}
    Planet *Jupiter(){return &mJupiter;}
    Planet *Saturn(){return &mSaturn;}
    Planet *Uranus(){return &mUranus;}
    Planet *Neptune(){return &mNeptune;}
    Planet *Pluto(){return &mPluto;}
    Planet *TrueNode(){return &mTrueNode;}
    Planet *MeanNode(){return &mMeanNode;}
    Planet *Earth(){return &mEarth;}

private:
    Planet mSun{SE_SUN};
    Planet mMercury{SE_MERCURY};
    Planet mVenus{SE_VENUS};
    Planet mMoon{SE_MOON};
    Planet mMars{SE_MARS};
    Planet mJupiter{SE_JUPITER};
    Planet mSaturn{SE_SATURN};
    Planet mUranus{SE_URANUS};
    Planet mNeptune{SE_NEPTUNE};
    Planet mPluto{SE_PLUTO};
    Planet mTrueNode{SE_TRUE_NODE};
    Planet mMeanNode{SE_MEAN_NODE};
    Planet mEarth{SE_EARTH};

    QStringList mErrors;
};

#endif // EPHEMERIS_H
