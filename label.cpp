#include <QDate>
#include <QDebug>
#include <QPainter>
#include <QPen>

#include "label.h"

LabelImpl::LabelImpl(qreal width, qreal height, QVariant value, QGraphicsItem *parent)
    : QGraphicsRectItem(-width/2., -height/2., width, height, parent), mValue(value)
{
    setPen(QPen(Qt::gray, 0.2));
    setFlag(GraphicsItemFlag::ItemIgnoresTransformations);
}

void LabelImpl::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setPen(pen());
    painter->setBrush(Qt::yellow);
    painter->drawRoundedRect(rect(), 5, 5);

    QString text;

    if(mValue.userType() == QMetaType::QString)
    {
        text = mValue.toString();
    }
    else if(mValue.userType() == QMetaType::QDate)
    {
        text = mValue.toDate().toString("d MMM");
    }

    painter->drawText(rect(), Qt::AlignCenter, text);
}

const QVariant &LabelImpl::value() const
{
    return mValue;
}

Label::Label(QPointF center, qreal fromRing, qreal width, qreal height, QVariant value, QGraphicsItem *parent)
    : QGraphicsItem(parent), mLabel(width, height, value, this), mDefaultPos(center), mShift(fromRing)
{
    QLineF line({}, center);
    line.setLength(line.length() + fromRing);

    setPos(line.p2());
}

QRectF Label::boundingRect() const
{
    return mLabel.boundingRect();
}

void Label::paint(QPainter *, const QStyleOptionGraphicsItem *, QWidget *)
{

}

bool Label::collidesWithItemTransform(const QGraphicsItem *other, QTransform t) const
{
    QRectF r1 = mLabel.boundingRect(), r2;
    const Label* lb = qgraphicsitem_cast<const Label*>(other);

    if(lb)
    {
        r2 = lb->boundingRect();

        QPointF p1 = pos(), p2 = lb->pos();

        if(!t.isIdentity())
        {
            p1 = t.map(p1);
            p2 = t.map(p2);
        }

        r1.moveTo(p1);
        r2.moveTo(p2);

        QPointF centerR1 = r1.center();
        QPointF centerR2 = r2.center();

        r1.setHeight(r1.height() * 1.25);
        r1.moveCenter(centerR1);

        r2.setHeight(r2.height() * 1.25);
        r2.moveCenter(centerR2);

        return r1.intersects(r2) || r1.contains(r2);
    }

    return false;
}

QPointF Label::getDefaultPosition() const
{
    return mDefaultPos;
}

const QVariant &Label::getValue() const
{
    return mLabel.value();
}

qreal Label::getShift() const
{
    return mShift;
}
