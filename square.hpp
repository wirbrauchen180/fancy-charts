#ifndef SQUARE_HPP
#define SQUARE_HPP

#include <QDate>
#include <QPen>
#include <QTime>
#include <QAbstractGraphicsShapeItem>

template<typename T>
class Square : public QAbstractGraphicsShapeItem
{
public:
    Square(int ceilSize, int size, T startValue, T nextValue, QString format = "", QGraphicsItem *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;
    QRectF boundingRect() const override;

private:
    int mCeilSize;
    int mSize;
    T mStartValue;
    T mNextValue;
    QString mFormat;

    QString toText(T value);
};

template<typename T>
Square<T>::Square(int ceilSize, int size, T startValue, T nextValue, QString format, QGraphicsItem *parent) : QAbstractGraphicsShapeItem(parent),
    mCeilSize(ceilSize), mSize(size), mStartValue(startValue), mNextValue(nextValue), mFormat(format)
{

}

template<typename T>
void Square<T>::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(mSize%2)//Square Of 9
    {

    }
    else //Square Of 4
    {

    }
}

template<typename T>
QRectF Square<T>::boundingRect() const
{
    QRectF r(-mCeilSize * mSize/2. - pen().widthF()/2., -mCeilSize * mSize/2  - pen().widthF()/2.,
              mCeilSize * mSize + pen().widthF()/2., mCeilSize * mSize + pen().widthF()/2.);
    return r;
}

template<typename T>
QString Square<T>::toText(T value)
{
    return QString::number(value);
}

template<>
QString Square<QDate>::toText(QDate value)
{
    return value.toString(mFormat);
}

template<>
QString Square<QTime>::toText(QTime value)
{
    return value.toString(mFormat);
}

#endif // SQUARE_HPP
