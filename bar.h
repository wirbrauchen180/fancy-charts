#ifndef BAR_H
#define BAR_H

#include <QDateTime>

struct Bar
{
    QDateTime datetime;
    double Open;
    double High;
    double Low;
    double Close;
    double Volume;
};

QDataStream &operator<<(QDataStream &dataStream, const Bar &bar);
QDataStream &operator>>(QDataStream &dataStream, Bar &bar);

#endif // BAR_H
