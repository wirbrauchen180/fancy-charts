#ifndef FRACTALTRENDINDICATOR_H
#define FRACTALTRENDINDICATOR_H

#include "qcustomplot.h"
#include "bar.h"
#include "colorselectwindow.h"

class FractalTrendIndicator
{
public:
	FractalTrendIndicator(QVector<Bar> &bars,  QCPAxis *keyAxis, QCPAxis *valueAxis, NamesAndColors = NamesAndColors());
};

#endif // FRACTALTRENDINDICATOR_H
