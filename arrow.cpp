#include <QBrush>
#include <qmath.h>
#include <QPen>

#include "arrow.h"

Arrow::Arrow(qreal x1, qreal y1, qreal x2, qreal y2, QGraphicsItem *parent) : QGraphicsPathItem(parent), mStartPoint(x1,y1), mEndPoint(x2, y2)
{
    recalculatePath();

    setBrush(QBrush(QColor(255, 53, 94)));
    setPen(QPen(QColor(255, 53, 94), 0.5));
}

void Arrow::setLength(qreal length)
{
    QLineF line(mStartPoint, mEndPoint);
    line.setLength(length);
    mEndPoint = line.p2();
    recalculatePath();
}

QPointF Arrow::endPoint() const
{
    return mEndPoint;
}

void Arrow::recalculatePath()
{
    qreal length = 32;
    qreal width = 5;

    QLineF line1(mEndPoint, mStartPoint);
    QLineF line2(mEndPoint, mStartPoint);

    line1.setLength(length);
    line2.setLength(length);

    line1.setAngle(line1.angle() - qRadiansToDegrees(qAtan2(width, length)));
    line2.setAngle(line2.angle() + qRadiansToDegrees(qAtan2(width, length)));

    QPainterPath path;
    path.moveTo(mStartPoint);
    path.lineTo(mEndPoint);
    path.closeSubpath();
    path.moveTo(mEndPoint);
    path.lineTo(line1.p2());
    path.quadTo(mEndPoint, line2.p2());
    path.closeSubpath();
    setPath(path);
}
