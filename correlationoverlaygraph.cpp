#include "correlationoverlaygraph.h"

#include "ui_correlationoverlaygraph.h"
#include "csvgraphdialog.h"
#include "correlation.h"
#include "correlationgraph.h"
#include "cpaxiscustomdatetimeticker.h"

#include "common.h"

#include <QDebug>
#include <QMessageBox>

CorrelationOverlayGraph::CorrelationOverlayGraph(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::CorrelationOverlayGraph)
{
	ui->setupUi(this);
	ui->fileLineEdit->installEventFilter(this);

	connect(ui->fileButton, &QPushButton::clicked, this, &CorrelationOverlayGraph::openFile);
	connect(ui->windowSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &CorrelationOverlayGraph::sizeChanged);

	connect(ui->splitSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), this, &CorrelationOverlayGraph::sizeChanged);

	connect(ui->firstDateEdit, &QDateTimeEdit::dateChanged, this, &CorrelationOverlayGraph::sizeChanged);
	connect(ui->secondDateEdit, &QDateTimeEdit::dateChanged, this, &CorrelationOverlayGraph::sizeChanged);

	connect(ui->openCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);
	connect(ui->closeCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);
	connect(ui->highCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);
	connect(ui->lowCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);
	connect(ui->averageCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);

	connect(ui->enableCheckBox, &QCheckBox::checkStateChanged, this, &CorrelationOverlayGraph::checkPriceCheckboxesChanged);

	connect(ui->actionCorrelationGraph, &QAction::triggered, this, &CorrelationOverlayGraph::ShowCorrelationGraph);
	connect(ui->calculateButton, &QPushButton::clicked, this, &CorrelationOverlayGraph::calculateBestCorrelation);
	connect(ui->nSpinBox, &QSpinBox::valueChanged, this, &CorrelationOverlayGraph::calculateBestCorrelation);
}

CorrelationOverlayGraph::~CorrelationOverlayGraph()
{
	delete ui;
}

void CorrelationOverlayGraph::setLimits()
{
	int wMaxSize = bars.size() / 2;
	ui->windowSpinBox->setMaximum(std::min(wMaxSize, 1000));
	ui->windowSpinBox->setMinimum(std::min(10, wMaxSize));

	ui->splitSpinBox->setMaximum(ui->windowSpinBox->value());

	QDateTime minDate1 = bars.first().datetime;
	QDateTime minDate2 = bars.at(ui->windowSpinBox->value()).datetime;

	QDateTime maxDate1 = bars.at(bars.size() - 1 - 2 * ui->windowSpinBox->value()).datetime;
	QDateTime maxDate2 = bars.at(bars.size() - 1 - ui->windowSpinBox->value()).datetime;

	ui->firstDateEdit->setMinimumDateTime(minDate1);
	ui->firstDateEdit->setMaximumDateTime(maxDate1);

	ui->secondDateEdit->setMinimumDateTime(minDate2);
	ui->secondDateEdit->setMaximumDateTime(maxDate2);
}

void CorrelationOverlayGraph::openFile()
{
	CSVGraphDialog dlg(this);
	if(dlg.exec() == QDialog::Accepted)
	{
		bars = dlg.getBars();
		setLimits();
		refreshChart();
	}
}

void CorrelationOverlayGraph::refreshChart()
{
	ui->customPlot->clearItems();
	ui->customPlot->clearPlottables();

	QDateTime dt = ui->firstDateEdit->dateTime();

	std::size_t firstChartStart = std::distance(std::begin(bars),
									std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	dt = ui->secondDateEdit->dateTime();

	std::size_t secondChartStart = std::distance(std::begin(bars),
									 std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	QColor n = QColor::fromString("#7f85535f"),
			p = QColor::fromString("#7f998fe8");

	drawChart(firstChartStart, ui->windowSpinBox->value(), ChartSettings{n, p, "", ChartIndex::kChartFirst});

	n = QColor::fromString("#7fcb4f02");
	p = QColor::fromString("#7ff2031e");

	drawChart(secondChartStart, ui->windowSpinBox->value(), ChartSettings{n, p, "", ChartIndex::kChartSecond});

	calculateCorrelation();
}

void CorrelationOverlayGraph::checkPriceCheckboxesChanged()
{
	if(!ui->highCheckBox->isChecked() && !ui->lowCheckBox->isChecked() &&
	   !ui->openCheckBox->isChecked() && !ui->closeCheckBox->isChecked())
	{
		ui->closeCheckBox->setChecked(true);
	}

	if(!ui->enableCheckBox->isChecked())
		ui->splitSpinBox->setEnabled(false);
	else
		ui->splitSpinBox->setEnabled(true);

	calculateCorrelation();
}

void CorrelationOverlayGraph::ShowCorrelationGraph()
{
	QDateTime dt = ui->firstDateEdit->dateTime();

	std::size_t firstChartStart = std::distance(std::begin(bars),
									std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	dt = ui->secondDateEdit->dateTime();

	std::size_t secondChartStart = std::distance(std::begin(bars),
									 std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	CorrelationGraph *g = new CorrelationGraph(GetCorrelationMovingWindow(bars, firstChartStart, secondChartStart - firstChartStart, ui->windowSpinBox->value(),
												  GetCorrelationType(), kPearson));

	g->show();
}

void CorrelationOverlayGraph::sizeChanged()
{
	setLimits();

	if( qobject_cast<QSpinBox*>(sender()) == ui->windowSpinBox)
		ui->splitSpinBox->setValue(ui->windowSpinBox->value()/2);

	refreshChart();
}

void CorrelationOverlayGraph::calculateBestCorrelation()
{
	static auto correlations = GetBestCorrelations(bars, 0, ui->windowSpinBox->value(),GetCorrelationType(), false);

	std::sort(std::begin(correlations),std::end(correlations),
		[](const auto& le, const auto &re)
		{
			return std::abs(le.value) > std::abs(re.value);
		});

	auto nth = 0;
	if(ui->nSpinBox->isEnabled())
	{
		nth += ui->nSpinBox->value();
	}

	std::nth_element(std::begin(correlations), std::begin(correlations) + nth, std::end(correlations),
		[](const auto& le, const auto &re)
		{
			return std::abs(le.value) > std::abs(re.value);
		});

	ui->firstDateEdit->setDateTime(correlations[nth].dateTime1);
	ui->secondDateEdit->setDateTime(correlations[nth].dateTime2);

	sizeChanged();

	ui->nSpinBox->setMinimum(0);
	ui->nSpinBox->setMaximum(correlations.size());
	ui->nSpinBox->setEnabled(true);
}

void CorrelationOverlayGraph::drawChart(std::size_t startPoint, std::size_t size, ChartSettings settings)
{
	QVector<Bar> data_bars = bars.mid(startPoint, size);

	QCPAxis *xAxis = settings.chartIdx == ChartIndex::kChartFirst ? ui->customPlot->xAxis : ui->customPlot->xAxis2,
			*yAxis = settings.chartIdx == ChartIndex::kChartFirst ? ui->customPlot->yAxis : ui->customPlot->yAxis2;

	QCPFinancial *ohlc = new QCPFinancial(xAxis, yAxis);

	ohlc->setName(settings.Title);
	ohlc->setChartStyle(QCPFinancial::csCandlestick);
	QCPFinancialDataContainer data;
	QSharedPointer<CPAxisCustomDateTimeTicker> tickerText(new CPAxisCustomDateTimeTicker(toDateTime(data_bars)));

	int idx = 0;

	for(Bar b : data_bars)
	{
		data.add(QCPFinancialData(static_cast<double>(idx), b.Open, b.High, b.Low, b.Close));
		//tickerText->addTick(static_cast<double>(idx), b.DateTime.toString("dd.MM.yyyy"));
		++idx;
	}

	ohlc->data()->set(data);
	ohlc->setWidth(0.2);
	ohlc->setTwoColored(true);

	ohlc->setPenNegative(QPen(settings.IncColor));
	ohlc->setPenPositive(QPen(settings.DecColor));

	ohlc->setBrushNegative(QBrush(settings.IncColor));
	ohlc->setBrushPositive(QBrush(settings.DecColor));

	xAxis->setBasePen(QPen(Qt::black));
	xAxis->setTicker(tickerText);
	xAxis->rescale();
	yAxis->rescale();
	xAxis->scaleRange(1.025, xAxis->range().center());
	yAxis->scaleRange(1.1, yAxis->range().center());

	if(settings.chartIdx != ChartIndex::kChartFirst)
	{
		xAxis->setVisible(true);
		yAxis->setVisible(true);
	}

	ui->customPlot->replot();
}

void CorrelationOverlayGraph::calculateCorrelation()
{
	QDateTime dt = ui->firstDateEdit->dateTime();

	std::size_t firstChartStart = std::distance(std::begin(bars),
									std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	QVector<Bar> a = bars.mid(firstChartStart, ui->windowSpinBox->value());

	dt = ui->secondDateEdit->dateTime();

	std::size_t secondChartStart = std::distance(std::begin(bars),
									 std::find_if(std::begin(bars), std::end(bars), [dt](Bar &a){return a.datetime >= dt;}));

	QVector<Bar> b = bars.mid( secondChartStart, ui->windowSpinBox->value());

	int correlationType = GetCorrelationType();

	QVector<QVector<int>> splits = ui->enableCheckBox->isChecked() ? SplitWindow(ui->windowSpinBox->value(), ui->splitSpinBox->value()) : QVector<QVector<int>>{{ui->windowSpinBox->value()}};

	QPair<std::size_t, double> k_p_results = GetCorrelationWithInverseParts(a, b, splits, kPearson, correlationType);
	QPair<std::size_t, double> k_s_results = GetCorrelationWithInverseParts(a, b, splits, kSpearman, correlationType);

	qDebug() << GetCorrelation(a, b, kPearson, correlationType) << " " << GetCorrelation(a, b, kSpearman, correlationType);

	if(k_p_results.second < 0)
		ui->lcdPearson->setDigitCount(7);
	else
		ui->lcdPearson->setDigitCount(6);

	ui->lcdPearson->display(k_p_results.second);

	if(k_s_results.second < 0)
		ui->lcdSpearman->setDigitCount(7);
	else
		ui->lcdSpearman->setDigitCount(6);

	ui->lcdSpearman->display(k_s_results.second);

	if(splits.at(k_p_results.first).size() > 1)
	{
		QCPItemRect *rect = new QCPItemRect(ui->customPlot);
		rect->topLeft->setCoords(splits.at(k_p_results.first).at(0), 100);
		rect->bottomRight->setCoords(splits.at(k_p_results.first).at(0) + splits.at(k_p_results.first).at(1), 0);
		rect->setPen(QPen(Qt::black));
	}
}

int CorrelationOverlayGraph::GetCorrelationType()
{
	int correlationType = 0;

	if(ui->averageCheckBox->isChecked())
		correlationType |= kAverage;
	if(ui->openCheckBox->isChecked())
		correlationType |= kOpen;
	if(ui->closeCheckBox->isChecked())
		correlationType |= kClose;
	if(ui->highCheckBox->isChecked())
		correlationType |= kHigh;
	if(ui->lowCheckBox->isChecked())
		correlationType |= kLow;

	return correlationType;
}
