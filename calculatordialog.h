#ifndef CALCULATORDIALOG_H
#define CALCULATORDIALOG_H

#include <QDialog>

#include "calculatorscene.h"

struct Parameters
{
    CalculatorType type;
    bool clockwise;
    int size;
};

namespace Ui {
class CalculatorDialog;
}

class CalculatorDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CalculatorDialog(CalculatorType type, bool clockwise, int size, QWidget *parent = nullptr);
    ~CalculatorDialog();

public slots:
    void apply();
    void accept();

signals:
    void typeChanged(CalculatorType);
    void directionChanged(bool);
    void sizeChanged(int);

    void rebuild();

private:
    Ui::CalculatorDialog *ui;

    Parameters mParam;
};

#endif // CALCULATORDIALOG_H
