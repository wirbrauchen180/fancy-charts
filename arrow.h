#ifndef ARROW_H
#define ARROW_H

#include <QGraphicsPathItem>

class Arrow : public QGraphicsPathItem
{
public:
    Arrow(qreal x1, qreal y1, qreal x2, qreal y2, QGraphicsItem *parent = nullptr);

    void setLength(qreal length);
    QPointF endPoint() const;

private:
    QPointF mStartPoint;
    QPointF mEndPoint;

    void recalculatePath();
};

#endif // ARROW_H
