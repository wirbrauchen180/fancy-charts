#include "cpaxiscustomdatetimeticker.h"
#include "cpfinancial.h"
#include "fractaltrendindicator.h"

#include "common.h"

CPFinancial::CPFinancial(QString title, QVector<Bar> &barsData, QCPAxis *kAxis, QCPAxis *vAxis, NamesAndColors colors) : QCPFinancial(kAxis, vAxis), bars(barsData)
{
    int idx = 0;

    std::sort(std::begin(bars), std::end(bars), [](Bar &a, Bar &b){return a.datetime <  b.datetime;});

    setName(title);
    setChartStyle(QCPFinancial::csCandlestick);
    QCPFinancialDataContainer dt;    
	QSharedPointer<CPAxisCustomDateTimeTicker> tickerText(new CPAxisCustomDateTimeTicker(toDateTime(bars)));

    for(Bar &b : bars)
    {
        dt.add(QCPFinancialData(static_cast<double>(idx), b.Open, b.High, b.Low, b.Close));       
        ++idx;
    }

    data()->set(dt);
    setSelectable(QCP::stWhole);

    keyAxis()->setBasePen(QPen(Qt::black));
    keyAxis()->setTicker(tickerText);

    keyAxis()->rescale();
    valueAxis()->rescale();

    int size = qMin(360, dt.size());

    int startIdx = dt.size() - 1 - size;
    int stopIdx = dt.size() - 1;

    connect(keyAxis(), QOverload<const QCPRange&>::of(&QCPAxis::rangeChanged), this, [this](const QCPRange &newRange)
    {
        if(newRange.lower < -1)
        {
            this->keyAxis()->setRange(QCPRange(0, newRange.upper));
        }        
        else if(newRange.lower >= bars.size())
        {
            this->keyAxis()->setRange(QCPRange(bars.size() - 1, newRange.upper));
        }

        this->rescaleY(this->keyAxis()->range());
    });

    keyAxis()->setRange(startIdx, stopIdx);

    if(colors.size() != ColorsCount)
    {
        QSettings settings;
        settings.beginGroup("CPFinancial");

        QColor color = settings.value("bodyPositiveColor", QColor(38, 166, 154, 120)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Body Positive", color));

        color = settings.value("borderPositiveColor", QColor(38, 166, 154)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Border Positive", color));

        color = settings.value("bodyNegativeColor", QColor(239, 83, 80, 120)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Body Negative", color));

        color = settings.value("borderNegativeColor", QColor(239, 83, 80)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Border Negative", color));

        color = settings.value("bodySelectedColor", QColor(3, 248, 252, 120)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Body Selected", color));

        color = settings.value("borderSelectedColor", QColor(3, 248, 252)).value<QColor>();
        colorsData.push_back(QPair<QString, QColor>("Border Selected", color));

        settings.endGroup();
    }
    else
        colorsData = colors;

    setWidth(0.333);
    setTwoColored(true);

	//new FractalTrendIndicator(bars, keyAxis(), valueAxis());

    repaint();
}

NamesAndColors &CPFinancial::getColorsData()
{
    return colorsData;
}

CPFinancial::~CPFinancial()
{

}

void CPFinancial::trendLine(int /*n*/)
{
    /*double currentPrice = 0;
    bool lowFirst;

    for(int i = 0; i < bars.size() - n; ++i)
    {

    }*/
}

void CPFinancial::rescaleY(const QCPRange &range)
{
    int upper = qMin(int(std::ceil(range.upper)), bars.size());

    double max_y = (*std::max_element(std::next(std::begin(bars), int(range.lower)), std::next(std::begin(bars), upper), [](Bar &a, Bar &b){return a.High < b.High;})).High;
    double min_y = (*std::min_element(std::next(std::begin(bars), int(range.lower)), std::next(std::begin(bars), upper), [](Bar &a, Bar &b){return a.Low < b.Low;})).Low;

    valueAxis()->setRange(0.999 * min_y, 1.001 * max_y);
}

void CPFinancial::repaint()
{
    setPenNegative(colorsData.at(ColorOrder::BorderNegative).second);
    setPenPositive(colorsData.at(ColorOrder::BorderPositive).second);

    setBrushNegative(colorsData.at(ColorOrder::BodyNegative).second);
    setBrushPositive(colorsData.at(ColorOrder::BodyPositive).second);

    selectionDecorator()->setPen(colorsData.at(ColorOrder::BorderSelected).second);
    selectionDecorator()->setBrush(colorsData.at(ColorOrder::BodySelected).second);

    QSettings settings;
    settings.beginGroup("CPFinancial");
    settings.setValue("bodyPositiveColor", colorsData.at(ColorOrder::BodyPositive).second);
    settings.setValue("borderPositiveColor", colorsData.at(ColorOrder::BorderPositive).second);
    settings.setValue("bodyNegativeColor", colorsData.at(ColorOrder::BodyNegative).second);
    settings.setValue("borderNegativeColor", colorsData.at(ColorOrder::BorderNegative).second);
    settings.setValue("bodySelectedColor", colorsData.at(ColorOrder::BodySelected).second);
    settings.setValue("borderSelectedColor", colorsData.at(ColorOrder::BorderSelected).second);
    settings.endGroup();

    parentPlot()->replot();
}

QDataStream &operator<<(QDataStream &dataStream, const CPFinancial &cpf)
{
    dataStream << cpf.bars << cpf.colorsData;
    return dataStream;
}

QDataStream &operator>>(QDataStream &dataStream, CPFinancial &cpf)
{
    dataStream >> cpf.bars >> cpf.colorsData;
    return dataStream;
}
