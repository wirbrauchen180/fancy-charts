#ifndef CALCULATORSCENE_H
#define CALCULATORSCENE_H

#include <QGraphicsScene>

#include "colorselectwindow.h"
#include "ringoverlay.h"
#include "squareceil.h"

enum CalculatorType
{
    kSquare4 = 0,
    kSquare9
};

class CalculatorScene : public QGraphicsScene
{
    Q_OBJECT

public:
    enum ColorOrder
    {
        kEmpty,
        kEmptyHover,
        kHigh,
        kHighHover,
        kLow,
        kLowHover,
        kSelected,
        kSelectedHover,

        kColorsCount
    };

    enum SceneMode
    {
        kFree,
        kMarkerAny,
        kMarkerHigh,
        kMarkerLow,
        kMarkerSelected
    };

    explicit CalculatorScene(QObject *parent = nullptr);

    QVariant startPoint;
    QVariant step;

    void clearOverlays(QTransform t = {});

    CalculatorType type() const;
    int size() const;
    bool clockwise() const;

    QColor GetColorAt(ColorOrder co) const;
    SceneMode getMode() const;

    NamesAndColors &getColors();

public slots:
    void sizeChanged(int newSize);
    void directionChanged(bool newDir);
    void typeChanged(CalculatorType type);
    void rebuild();
    void changeMode(CalculatorScene::SceneMode mode);
    void recolor();

private:
    CalculatorType mType = kSquare9;
    int mSize = 19;
    bool mClockwise = true;
    QVector<RingOverlay *> mOverlays;
    QVector<SquareCeil*> mCeils;
    NamesAndColors colors;
    SceneMode mMode = kFree;
};

#endif // CALCULATORSCENE_H
