#include <QDebug>

#include <QDialogButtonBox>
#include <QLineEdit>
#include <QPushButton>

#include "calculatordialog.h"
#include "ui_calculatordialog.h"

CalculatorDialog::CalculatorDialog(CalculatorType type, bool clockwise, int size, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CalculatorDialog), mParam({type, clockwise, size})
{
    ui->setupUi(this);

	auto lineEdit = ui->sizeSpinBox->findChild<QLineEdit*>();
    lineEdit->setReadOnly(true);
    lineEdit->setFocusPolicy(Qt::NoFocus);

    connect(ui->buttonBox->button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &CalculatorDialog::apply );
    connect(ui->buttonBox->button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &CalculatorDialog::accept );

    ui->typeComboBox->setCurrentIndex(type);
    ui->directionCheckBox->setChecked(clockwise);
	ui->sizeSpinBox->setValue(size);
	ui->sizeSpinBox->setMinimum(2 + type);
	ui->sizeSpinBox->setMaximum(1000 + type);

	connect(ui->sizeSpinBox, QOverload<int>::of(&QSpinBox::valueChanged), ui->sizeSpinBox,
                [&, lineEdit](){lineEdit->deselect();}, Qt::QueuedConnection);

    connect(ui->typeComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            [this](int idx){
								int newValue = ui->sizeSpinBox->value() + (idx ? 1 : -1);
								ui->sizeSpinBox->setMinimum(2 + idx);
								ui->sizeSpinBox->setMaximum(1000 + idx);
                                newValue = qMax(newValue, 2 + idx);
                                newValue = qMin(newValue, 1000 + idx);
								ui->sizeSpinBox->setValue(newValue);
                            });
}

CalculatorDialog::~CalculatorDialog()
{
    delete ui;
}

void CalculatorDialog::apply()
{
    bool toRebuild = false;

	if(ui->sizeSpinBox->value() != mParam.size)
    {
		emit sizeChanged(ui->sizeSpinBox->value());
        toRebuild = true;
    }

    if(ui->directionCheckBox->isChecked() != mParam.clockwise)
    {
        emit directionChanged(ui->directionCheckBox->isChecked());
        toRebuild = true;
    }

    if(ui->typeComboBox->currentIndex() != mParam.type)
    {
        emit typeChanged(CalculatorType(ui->typeComboBox->currentIndex()));
        toRebuild = true;
    }

    if(toRebuild)
    {
        emit rebuild();
		mParam = Parameters{CalculatorType(ui->typeComboBox->currentIndex()), ui->directionCheckBox->isChecked(), ui->sizeSpinBox->value()};
    }
}

void CalculatorDialog::accept()
{
    apply();
    QDialog::accept();
}
