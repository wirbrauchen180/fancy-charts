#ifndef CPAXISCUSTOMDATETIMETICKER_H
#define CPAXISCUSTOMDATETIMETICKER_H

#include "bar.h"
#include "qcustomplot.h"

enum TimeFrame
{
    tfUnknown,
    tfMinute,
    tfHour,
    tfDay,
    tfWeek,
    tfMonth,
    tfYear
};

enum Priority
{
    prDefault,
    prHour,
    prDay,
    prMonth,
    prYear
};

class CPAxisCustomDateTimeTicker : public QCPAxisTicker
{
public:
	explicit CPAxisCustomDateTimeTicker(const QVector<QDateTime> &bars);

    virtual void generate (const QCPRange &range, const QLocale &locale, QChar formatChar,
                           int precision, QVector< double > &ticks, QVector< double > *subTicks,
                           QVector< QString > *tickLabels) override;

protected:
    virtual double 	getTickStep (const QCPRange &range) override;
    virtual QVector<double> createTickVector(double tickStep, const QCPRange &range) override;

private:
    TimeFrame calculateTimeFrame();
    Priority getTickPriority(int index);
    QString createLabel(int index, bool firstOrLast = false);    
    void cleanTicks(double step, QVector<double> &ticks, QVector<QString> &labels);

    QCPRange existingRange(const QCPRange &range);

	QVector<QDateTime> mBars;
};

#endif // CPAXISCUSTOMDATETIMETICKER_H
