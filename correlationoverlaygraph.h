#pragma once

#include "correlation.h"
#include "qcustomplot.h"

#include <QMainWindow>

namespace Ui {
class CorrelationOverlayGraph;
}

struct ChartSettings
{
	QColor IncColor;
	QColor DecColor;
	QString Title;
	ChartIndex chartIdx;
};

class CorrelationOverlayGraph : public QMainWindow
{
	Q_OBJECT

public:
	explicit CorrelationOverlayGraph(QWidget *parent = nullptr);
	~CorrelationOverlayGraph();

private:
	Ui::CorrelationOverlayGraph *ui;

	QVector<Bar> bars;
	void setLimits();

	void drawChart(std::size_t startPoint, std::size_t size, ChartSettings settings);
	void calculateCorrelation();

	int GetCorrelationType();


private slots:
	void openFile();
	void refreshChart();
	void checkPriceCheckboxesChanged();

	void ShowCorrelationGraph();

	void sizeChanged();
	void calculateBestCorrelation();

};
