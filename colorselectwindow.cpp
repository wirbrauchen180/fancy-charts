#include <QPushButton>
#include <QColorDialog>

#include "colorselectwindow.h"
#include "ui_colorselectwindow.h"

ColorSelectWindow::ColorSelectWindow(NamesAndColors &data, QWidget *parent) :
    QDialog(parent), ui(new Ui::ColorSelectWindow), colorsData(data)
{
    ui->setupUi(this);

    int row = 0;

    for(QPair<QString, QColor> &p : colorsData)
    {
        ui->colorTableWidget->insertRow(row);
        auto itemToAdd = new QTableWidgetItem();
        itemToAdd->setData(Qt::DisplayRole, p.first);
        itemToAdd->setFlags(itemToAdd->flags() & (~Qt::ItemIsEditable));
        ui->colorTableWidget->setItem(row, 0, itemToAdd);

        QPushButton *button = new QPushButton();
        button->setProperty("row", row);
        button->setStyleSheet(QString("background: %1").arg(p.second.name(QColor::HexArgb)));
        ui->colorTableWidget->setCellWidget(row++, 1, button);

        connect(button, &QPushButton::clicked, this, &ColorSelectWindow::changeColor);

        colors.push_back(p.second);
    }

    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, [this]()
    {
        int index = 0;

        for(QColor &c : colors)
        {
            colorsData[index++].second = c;
        }

        emit repaintParent();
        this->close();
    });

    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, [this]()
    {
        this->close();
    });
}

ColorSelectWindow::~ColorSelectWindow()
{
    delete ui;
}

void ColorSelectWindow::changeColor()
{
    QPushButton *button = qobject_cast<QPushButton*>(sender());

    if(button)
    {
        int index = button->property("row").toInt();
        QColor sc = QColorDialog::getColor(colors[index], this, QString(), QColorDialog::ShowAlphaChannel);
        if(sc.isValid())
        {
            colors[index] = sc;
            button->setStyleSheet(QString("background: %1").arg(colors[index].name(QColor::HexArgb)));
        }
    }
}
