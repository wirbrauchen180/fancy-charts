#pragma once

#include <QDateTime>
#include <QVector>

#include "bar.h"

enum CorrelationType
{
	kPearson,
	kSpearman
};

enum ChartIndex
{
	kChartFirst,
	kChartSecond
};

enum Price
{
	kOpen = 1,
	kClose = 2,
	kHigh = 4,
	kLow = 8,
	kAverage = 16
};

struct Correlation
{
	double value;
	QDateTime dateTime1;
	QDateTime dateTime2;
	std::size_t index1;
	std::size_t index2;
};

double GetCorrelation(QVector<Bar> &x, QVector<Bar> &y, CorrelationType type, int priceType);

bool readFile(QString fileName, QString format, QVector<Bar>&);

QVector<Correlation> GetCorrelationMovingWindow(QVector<Bar>&, std::size_t firstBar, std::size_t diff, std::size_t windowSize,
											  int priceType, CorrelationType correlationType, bool onlyForward = true);

QVector<Correlation> GetBestCorrelations(QVector<Bar>&, std::size_t firstBar, std::size_t windowSize, int priceType, bool onlyForward = true);

QVector<QVector<int>> SplitWindow(std::size_t windowSize, std::size_t minSize);

QPair<std::size_t, double> GetCorrelationWithInverseParts(QVector<Bar> &x, QVector<Bar> &y, QVector<QVector<int>> splitPoints, CorrelationType type, int priceType);
