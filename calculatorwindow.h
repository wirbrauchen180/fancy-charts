#ifndef CALCULATORWINDOW_H
#define CALCULATORWINDOW_H

#include <QMainWindow>
#include <QVariant>
#include <QGraphicsScene>

#include "calculatorscene.h"

namespace Ui {
class CalculatorWindow;
}
class CalculatorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit CalculatorWindow(QWidget *parent = nullptr);
    ~CalculatorWindow();

    void settingsWindow();
    void saveAsImage();

private:
    Ui::CalculatorWindow *ui;
    QSharedPointer<CalculatorScene> mScene;

private slots:
    void setColors();
};

#endif // CALCULATORWINDOW_H
