#include <QDir>
#include <QDebug>
#include <QStandardPaths>

#include "ephemeris.h"

Ephemeris::Ephemeris()
{
//    QDir epheDir(QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation));
//    if(epheDir.cd("ephe"))
//        swe_set_ephe_path(QDir::toNativeSeparators(epheDir.absolutePath()).toStdString().data());
//    else
//        swe_set_ephe_path(nullptr);
}

void Ephemeris::addErrors(QString error)
{
    mErrors << error;
}

void Ephemeris::clearErrors()
{
    mErrors.clear();
}

QStringList Ephemeris::getErrors()
{
    return mErrors;
}

Ephemeris::~Ephemeris()
{
//    swe_close();
}

void Ephemeris::test()
{
//    QDate  dt = QDateTime::currentDateTime().date();
//    long iflag = SEFLG_SPEED | SEFLG_SWIEPH, iflgret;
//    double x2[6];
//    char serr[AS_MAXCH];
//    char snam[40];

//    double tjd_ut = swe_julday(dt.year(), dt.month(), dt.day(), 0.0, SE_GREG_CAL);

//    for (int p = SE_SUN; p <= SE_CHIRON; p++)
//    {
//        if (p == SE_EARTH)
//            continue;

//        iflgret = swe_calc_ut(tjd_ut, p, iflag, x2, serr);

//        if (iflgret < 0)
//            qDebug() << serr;
//        else
//        {
//            swe_get_planet_name(p, snam);
//            qDebug() << snam << x2[0] << x2[1] << x2[2] << x2[3];
//        }
//    }

//    double longitude = Sun()->Longitude(QDateTime::currentDateTimeUtc(), false);

//    if(std::isfinite(longitude))
//        qDebug() << "Sun: " << longitude;
//    else
//        qDebug() << mErrors;

//    longitude = Moon()->Longitude(QDateTime::currentDateTimeUtc(), false);

//    if(std::isfinite(longitude))
//        qDebug() << "Moon: " << longitude;
//    else
//        qDebug() << mErrors;

//    longitude = Saturn()->Longitude(QDateTime::currentDateTimeUtc(), false);

//    if(std::isfinite(longitude))
//        qDebug() << "Saturn: " << longitude;
//    else
//        qDebug() << mErrors;
}

Ephemeris *Ephemeris::getInstance()
{
    static Ephemeris ep;
    return &ep;
}

Planet::Planet(int planetIndex) : mPlanetIndex(planetIndex)
{

}

double Planet::Latitude(QDateTime dt, bool helio)
{
    PlanetData &data = helio ? mDataHelio : mDataGeo;

    if(!data.contains(dt))
        calc(dt, false);

    return data.contains(dt) ? data[dt].at(kLatitude) : std::numeric_limits<double>::quiet_NaN();
}

double Planet::Longitude(QDateTime dt, bool helio)
{
    PlanetData &data = helio ? mDataHelio : mDataGeo;

    if(!data.contains(dt))
        calc(dt, false);

    return data.contains(dt) ? data[dt].at(kLongitude) : std::numeric_limits<double>::quiet_NaN();
}

double Planet::speedLong(QDateTime dt, bool helio)
{
    PlanetData &data = helio ? mDataHelio : mDataGeo;

    if(!data.contains(dt))
        calc(dt, false);

    return data.contains(dt) ? data[dt].at(kSpeedLong) : std::numeric_limits<double>::quiet_NaN();
}

QDateTime Planet::At(double longitude, QDateTime after, bool helio)
{
    double precision = 0.001;
    double diff;
	//int inc = 86400;

    if(!after.isValid())
        after = QDateTime::currentDateTimeUtc();
    do
    {
        diff = longitude - Longitude(after, helio);
    }
    while(diff > precision);

    return after;
}

bool Planet::calc(QDateTime dt, bool helio)
{
   long iflag = SEFLG_SPEED | SEFLG_SWIEPH;

   if(helio)
	   iflag |= SEFLG_HELCTR;

   QString serr;
   serr.resize(AS_MAXCH);

   QVector<double> data(kSize);

   double time = dt.time().hour() + dt.time().minute() / 60. + dt.time().second() / 3600.;
   double tjd_ut = swe_julday(dt.date().year(), dt.date().month(), dt.date().day(), time, SE_GREG_CAL);

   long iflgret = swe_calc_ut(tjd_ut, mPlanetIndex, iflag, data.data(), serr.toStdString().data());

   if(iflgret < 0)
	   Ephemeris::getInstance()->addErrors(serr);
   else
   {
	   if(!helio)
		   mDataGeo.insert(dt, data);
	   else
		   mDataHelio.insert(dt, data);
   }

   return iflgret >= 0;
}
