#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMdiSubWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void addChart();
    void exitWindow();
    void saveAs(bool useCurrentFile = false);
    void open();
    void addSquareOf9();
	void addCorrelationGraph();

private:
    Ui::MainWindow *ui;

	QMdiSubWindow *createSubwindow(QWidget *widget, QString title);
};

#endif // MAINWINDOW_H
