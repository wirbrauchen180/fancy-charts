#ifndef LABEL_H
#define LABEL_H

#include <QGraphicsRectItem>

#include "common.h"

class LabelImpl : public QGraphicsRectItem
{
public:
    LabelImpl(qreal width, qreal height, QVariant value, QGraphicsItem *parent = nullptr);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem */*option*/, QWidget */*widget*/);

    const QVariant &value() const;

private:
    QVariant mValue;
};

class Label : public QGraphicsItem
{
public:
    Label(QPointF center, qreal fromRing, qreal width, qreal height, QVariant text, QGraphicsItem *parent = nullptr);

    QRectF boundingRect() const override;
    void paint (QPainter *, const QStyleOptionGraphicsItem *, QWidget *) override;
    bool collidesWithItemTransform(const QGraphicsItem *other, QTransform = {}) const;

	enum {Type = UserType + static_cast<int>(GraphicsItemType::kLabel)};

    int type() const override
    {
        return Type;
    }

    QPointF getDefaultPosition() const;
    const QVariant &getValue() const;
    qreal getShift() const;
private:
    LabelImpl mLabel;
    QPointF mDefaultPos;
    qreal mShift;
};

#endif // LABEL_H
