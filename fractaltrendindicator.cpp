#include "fractaltrendindicator.h"

FractalTrendIndicator::FractalTrendIndicator(QVector<Bar> &bars, QCPAxis *keyAxis, QCPAxis *valueAxis, NamesAndColors/* colors*/)
{
    QVector<double> timeBarLow,  timeBarHigh, priceBarLow, priceBarHigh;
    int size = 3;
    enum Direction
    {
        kNo,
        kUp,
        kDown
    };

    Direction dir = kNo;

    for(int i = size; i < bars.size() - size; ++i)
    {
        bool foundUp = true;

        for(int j = i - size; j != i; ++j)
        {
            if(bars.at(j).Low < bars.at(i).Low || (bars.at(j).Low == bars.at(i).Low && timeBarLow.contains(j)))
            {
                foundUp = false;
                break;
            }
        }

        for(int j = i + 1; j != i + size; ++j)
        {
            if(bars.at(j).Low < bars.at(i).Low || (bars.at(j).Low == bars.at(i).Low && timeBarLow.contains(j)))
            {
                foundUp = false;
                break;
            }
        }

        bool foundDown = true;

        for(int j = i - size; j != i; ++j)
        {
            if(bars.at(j).High > bars.at(i).High || (bars.at(j).High == bars.at(i).High && timeBarHigh.contains(j)))
            {
                foundDown = false;
                break;
            }
        }

        for(int j = i + 1; j != i + size; ++j)
        {
            if(bars.at(j).High > bars.at(i).High || (bars.at(j).High == bars.at(i).High && timeBarHigh.contains(j)))
            {
                foundDown = false;
                break;
            }
        }

        if(foundDown && !foundUp)
        {
            if(dir == kNo || dir == kUp)
            {
                dir = kDown;
                priceBarHigh.push_back(bars.at(i).High);
                timeBarHigh.push_back(i);
            }
            else
            {
                if(priceBarHigh.last() < bars.at(i).High)
                {
                    priceBarHigh.last() = bars.at(i).High;
                    timeBarHigh.last() = i;
                }
            }
        }
        else if(!foundDown && foundUp)
        {

            if(dir == kNo || dir == kDown)
            {
                dir = kUp;
                priceBarLow.push_back(bars.at(i).Low);
                timeBarLow.push_back(i);
            }
            else
            {
                if(priceBarLow.last() > bars.at(i).Low)
                {
                    priceBarLow.last() = bars.at(i).Low;
                    timeBarLow.last() = i;
                }
            }
        }
        else if(foundDown && foundUp)
        {
            if(dir != kNo)
            {
                priceBarLow.push_back(bars.at(i).Low);
                timeBarLow.push_back(i);

                priceBarHigh.push_back(bars.at(i).High);
                timeBarHigh.push_back(i);

                if(dir == kUp)
                    dir = kDown;
                else
                    dir = kUp;
            }
        }
    }

    for(int i = 0; i < priceBarLow.size(); ++i)
    {
        priceBarLow[i] = priceBarLow.at(i) * 0.999;
    }

    for(int i = 0; i < priceBarHigh.size(); ++i)
    {
        priceBarHigh[i] = priceBarHigh.at(i) * 1.001;
    }

    QCPGraph *highs = new QCPGraph(keyAxis, valueAxis);
    highs->setData(timeBarHigh, priceBarHigh, true);
    highs->setLineStyle(QCPGraph::LineStyle::lsNone);
    highs->setScatterStyle(QPixmap(":/arrowUp.png"));

    QCPGraph *lows = new QCPGraph(keyAxis, valueAxis);
    lows->setData(timeBarLow, priceBarLow, true);
    lows->setLineStyle(QCPGraph::LineStyle::lsNone);
    lows->setScatterStyle(QPixmap(":/arrowDown.png"));
}
