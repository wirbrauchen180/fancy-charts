#include <QDebug>
#include <QFontMetrics>
#include <QGraphicsView>
#include <qmath.h>

#include "arrow.h"
#include "calculatorscene.h"
#include "label.h"
#include "ringoverlay.h"

CalculatorScene::CalculatorScene(QObject *parent) : QGraphicsScene(parent)
{
    setItemIndexMethod(ItemIndexMethod::NoIndex);

    rebuild();

    colors.push_back(QPair<QString, QColor>("Empty", QColor(255, 255, 255)));
    colors.push_back(QPair<QString, QColor>("Empty Hover", QColor(206, 200, 239, 127)));
    colors.push_back(QPair<QString, QColor>("High", QColor(157, 224, 147)));
    colors.push_back(QPair<QString, QColor>("High Hover", QColor(157, 224, 147, 127)));
    colors.push_back(QPair<QString, QColor>("Low", QColor(254, 111, 94)));
    colors.push_back(QPair<QString, QColor>("Low Hover", QColor(254, 111, 94, 127)));
    colors.push_back(QPair<QString, QColor>("Selected", QColor(252, 214, 103)));
    colors.push_back(QPair<QString, QColor>("Selected Hover", QColor(252, 214, 103, 127)));
}

void CalculatorScene::clearOverlays(QTransform t)
{
    foreach(RingOverlay *ro, mOverlays)
        ro->clearLabels(t);
}

CalculatorType CalculatorScene::type() const
{
    return mType;
}

int CalculatorScene::size() const
{
    return mSize;
}

bool CalculatorScene::clockwise() const
{
    return mClockwise;
}

QColor CalculatorScene::GetColorAt(ColorOrder co) const
{
    return colors.at(co).second;
}

CalculatorScene::SceneMode CalculatorScene::getMode() const
{
    return mMode;
}

NamesAndColors &CalculatorScene::getColors()
{
    return colors;
}

void CalculatorScene::sizeChanged(int newSize)
{
    mSize = newSize;
}

void CalculatorScene::directionChanged(bool newDir)
{
    mClockwise = newDir;
}

void CalculatorScene::typeChanged(CalculatorType type)
{
    mType = type;
}

void CalculatorScene::rebuild()
{
    int squareSize = 32;

    if(mSize * mSize < mCeils.size())
    {
        int counter = 0;

        foreach(SquareCeil *item, mCeils)
        {
                if(counter++ >= mSize * mSize)
                {
                    removeItem(item);
                }
        }

        mCeils.erase(mCeils.begin() + mSize * mSize, mCeils.end());
    }

    startPoint = 1;
    QVariant count = startPoint;
    step = 1;
    int total = 0;

    mCeils.reserve(mSize * mSize);

    if(mType == CalculatorType::kSquare9)
    {
        enum Move
        {
            kLeft,
            kUp,
            kRight,
            kDown
        };

        Move mv = kLeft;
        int x = -squareSize/2, y = -squareSize/2, counter = 0, limit = 2;

        for(int i = 0; i < mSize * mSize; ++i, ++total)
        {
            QString text;

            if(count.userType() == QMetaType::Int)
            {
                text = QString::number(count.toInt());
                count = count.toInt() + step.toInt();
            }

            if(total >= mCeils.size())
            {
                SquareCeil *sq = new SquareCeil(x, y, squareSize, squareSize, text);
                addItem(sq);
                mCeils.push_back(sq);
            }
            else
            {
                mCeils.at(total)->setRect(x, y, squareSize, squareSize);
            }

            switch(mv)
            {
            case kLeft:
                x -= mClockwise ? squareSize : -squareSize;
                break;
            case kUp:
                y -= squareSize;
                break;
            case kRight:
                x += mClockwise ? squareSize : -squareSize;
                break;
            case kDown:
                y += squareSize;
                break;
            }

            ++counter;

            if(counter == limit || counter == limit/2)
            {
                switch(mv)
                {
                case kLeft:
                    mv = kUp;
                    break;
                case kUp:
                    mv = kRight;
                    break;
                case kRight:
                    mv = kDown;
                    break;
                case kDown:
                    mv = kLeft;
                    break;
                }
            }

            if(counter == limit)
            {
                limit += 2;
                counter = 0;
            }
        }
    }
    else if(mType == CalculatorType::kSquare4)
    {
        if(mSize%2)
            --mSize;

        enum Move
        {
            kRight,
            kDown,
            kLeft,
            kUp,
            kDiag
        };

        Move mv = kRight;
        int x = mClockwise ? -squareSize : 0, y = -squareSize, counter = 0, limit = 5;

        for(int i = 0; i < mSize * mSize; ++i, ++total)
        {
            QString text;

            if(count.userType() == QMetaType::Int)
            {
                text = QString::number(count.toInt());
                count = count.toInt() + step.toInt();
            }

            if(total >= mCeils.size())
            {
                SquareCeil *sq = new SquareCeil(x, y, squareSize, squareSize, text);
                addItem(sq);
                mCeils.push_back(sq);
            }
            else
            {
                mCeils.at(total)->setRect(x, y, squareSize, squareSize);
            }

            ++counter;

            switch(mv)
            {
            case kLeft:
            {
                x -= mClockwise ? squareSize : -squareSize;

                if(8 * (counter - 3) == 6 * (limit - 5))
                {
                    if(counter != 3)
                        mv = kUp;
                    else
                        mv = kDiag;
                }
            }
                break;
            case kUp:
            {
                y -= squareSize;

                if(counter == limit)
                {
                    mv = kRight;
                }
                else if(8 * (counter - 3) == 7 * (limit - 5))
                {
                    mv = kDiag;
                }
            }
                break;
            case kRight:
            {
                x += mClockwise ? squareSize : -squareSize;

                if(8 * (counter - 1) == 2 * (limit - 5))
                    mv = kDown;
            }
                break;
            case kDown:
            {
                y += squareSize;

                if(8 * (counter - 2) == 4 * (limit - 5))
                    mv = kLeft;
            }
                break;
            case kDiag:
            {
                y -= squareSize;
                x -= mClockwise ? squareSize : -squareSize;

                mv = kUp;
            }
                break;
            }

            if(counter == limit)
            {
                counter = 0;
                limit += 8;
            }
        }
    }

    foreach(RingOverlay *ro, mOverlays)
    {
        removeItem(ro);
    }
    mOverlays.clear();    
    auto ro = new RingOverlay(kDegrees, qSqrt(2) * squareSize * mSize/2., 50, 3., true);
    addItem(ro);
    ro->setZValue(-1);
    mOverlays.push_back(ro);
    ro = new RingOverlay(kDays, qSqrt(2) * squareSize * mSize/2., 175., 5, true);
    addItem(ro);
    ro->setZValue(-2);
    mOverlays.push_back(ro);

    clearOverlays();
}

void CalculatorScene::changeMode(SceneMode mode)
{
    mMode = mode;
}

void CalculatorScene::recolor()
{
    foreach(SquareCeil *sc, mCeils)
    {
        sc->initializeBrushes();
        sc->update(sc->rect());
    }
}
