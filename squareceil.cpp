#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QStyleOptionGraphicsItem>

#include "calculatorscene.h"
#include "squareceil.h"

SquareCeil::SquareCeil(qreal x, qreal y, qreal width, qreal height, QString text, QGraphicsItem *parent) : QGraphicsRectItem(x, y, width, height, parent), mText(text)
{
    setAcceptHoverEvents(true);
    setZValue(-5);
}

void SquareCeil::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(!mInitializedBrushes)
        initializeBrushes();

    QBrush brush;

    if(!mHover)
    {
        switch(mMarked)
        {
        case kNone:
            brush = mEmptyBrush;
            break;

        case kHigh:
            brush = mHighBrush;
            break;

        case kLow:
            brush = mLowBrush;
            break;

        case kSelected:
            brush = mSelectedBrush;
            break;

        default:
            break;
        }
    }
    else
    {
        switch(mMarked)
        {
        case kNone:
            brush = mEmptyHoverBrush;
            break;

        case kHigh:
            brush = mHighHoverBrush;
            break;

        case kLow:
            brush = mLowHoverBrush;
            break;

        case kSelected:
            brush = mSelectedHoverBrush;
            break;

        default:
            break;
        }
    }

    setBrush(brush);

    QGraphicsRectItem::paint(painter, option, widget);
    painter->drawText(rect(), Qt::AlignCenter, mText);
}

void SquareCeil::hoverEnterEvent(QGraphicsSceneHoverEvent */*event*/)
{
    mHover = true;
    update(rect());
}

void SquareCeil::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
    mHover = false;
    update(rect());
}

void SquareCeil::mousePressEvent(QGraphicsSceneMouseEvent *me)
{
    CalculatorScene *sc = qobject_cast<CalculatorScene *>(scene());

    if(sc->getMode() == CalculatorScene::kMarkerAny)
    {
        if((mMarked == kHigh && me->button() != Qt::LeftButton) ||
           (mMarked == kLow && me->button() != Qt::RightButton) ||
           (mMarked == kSelected && me->button() != Qt::MiddleButton))
        {
            mMarked = kNone;
        }
        else if(mMarked == kNone)
        {
            if(me->button() == Qt::LeftButton)
            {
                mMarked = kHigh;
                update(rect());
            }
            else if(me->button() == Qt::RightButton)
            {
                mMarked = kLow;
                update(rect());
            }
            else if(me->button() == Qt::MiddleButton)
            {
                mMarked = kSelected;
                update(rect());
            }
        }
    }
    else if(sc->getMode() == CalculatorScene::kMarkerHigh)
    {
        if(mMarked == kNone)
        {
            mMarked = kHigh;
            update(rect());
        }
        else
        {
            mMarked = kNone;
            update(rect());
        }
    }
    else if(sc->getMode() == CalculatorScene::kMarkerLow)
    {
        if(mMarked == kNone)
        {
            mMarked = kLow;
            update(rect());
        }
        else
        {
            mMarked = kNone;
            update(rect());
        }
    }
    else if(sc->getMode() == CalculatorScene::kMarkerSelected)
    {
        if(mMarked == kNone)
        {
            mMarked = kSelected;
            update(rect());
        }
        else
        {
            mMarked = kNone;
            update(rect());
        }
    }

}

void SquareCeil::initializeBrushes()
{
    CalculatorScene *sc = qobject_cast<CalculatorScene *>(scene());

    mEmptyBrush = QBrush(sc->GetColorAt(CalculatorScene::kEmpty));
    mEmptyHoverBrush = QBrush(sc->GetColorAt(CalculatorScene::kEmptyHover));
    mHighBrush = QBrush(sc->GetColorAt(CalculatorScene::kHigh));
    mHighHoverBrush = QBrush(sc->GetColorAt(CalculatorScene::kHighHover));
    mLowBrush = QBrush(sc->GetColorAt(CalculatorScene::kLow));
    mLowHoverBrush = QBrush(sc->GetColorAt(CalculatorScene::kLowHover));
    mSelectedBrush = QBrush(sc->GetColorAt(CalculatorScene::kSelected));
    mSelectedHoverBrush = QBrush(sc->GetColorAt(CalculatorScene::kSelectedHover));

    mInitializedBrushes = true;
}
