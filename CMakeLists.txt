cmake_minimum_required(VERSION 3.5)

project(FancyCharts VERSION 0.1 LANGUAGES C CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 REQUIRED COMPONENTS Widgets PrintSupport Concurrent)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Widgets PrintSupport Concurrent)

set(PROJECT_SOURCES
    arrow.cpp
    bar.cpp
    calculatordialog.cpp
    calculatorscene.cpp
    calculatorview.cpp
    calculatorwindow.cpp
    chartwindow.cpp
    colorselectwindow.cpp
    correlation.cpp
    correlationgraph.cpp
    correlationoverlaygraph.cpp
    cpaxiscustomdatetimeticker.cpp
    cpfinancial.cpp
    csvgraphdialog.cpp
    csvparser.cpp
    ephemeris.cpp
    fractaltrendindicator.cpp
    label.cpp
    main.cpp
    mainwindow.cpp
    overlaysdialog.cpp
    qcustomplot.cpp
    ringoverlay.cpp
    squareceil.cpp
)

set(PROJECT_HEADERS
    arrow.h
    calculatordialog.h
    calculatorscene.h
    calculatorview.h
    calculatorwindow.h
    chartwindow.h
    colorselectwindow.h
    common.h
    correlation.h
    correlationgraph.h
    correlationoverlaygraph.h
    cpaxiscustomdatetimeticker.h
    bar.h
    cpfinancial.h
    csvgraphdialog.h
    csvparser.h
    ephemeris.h
    fractaltrendindicator.h
    label.h
    mainwindow.h
    overlaysdialog.h
    qcustomplot.h
    ringoverlay.h
    square.hpp
    squareceil.h
    sweph/swedll.h
    version.h
)

set(PROJECT_UIS
    calculatordialog.ui
    calculatorwindow.ui
    chartwindow.ui
    colorselectwindow.ui
    correlationgraph.ui
    correlationoverlaygraph.ui
    csvgraphdialog.ui
    mainwindow.ui
    overlaysdialog.ui)

set(PROJECT_QRCS
    fancycharts.qrc)

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

qt_add_executable(FancyCharts
        MANUAL_FINALIZATION
        ${PROJECT_HEADERS} ${PROJECT_SOURCES} ${PROJECT_UIS} ${PROJECT_QRCS}
    )

target_compile_options(FancyCharts PRIVATE -Wall -Wextra -Wpedantic -Werror)

add_subdirectory(sweph)

target_link_libraries(FancyCharts PUBLIC Qt${QT_VERSION_MAJOR}::Concurrent
                                         Qt${QT_VERSION_MAJOR}::PrintSupport
                                  PRIVATE Qt${QT_VERSION_MAJOR}::Widgets
                                  SwephLib)
