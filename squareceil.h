#ifndef SQUARECEIL_H
#define SQUARECEIL_H

#include <QBrush>
#include <QGraphicsItem>

#include "common.h"

class SquareCeil : public QGraphicsRectItem, QObject
{
public:
    SquareCeil(qreal x, qreal y, qreal width, qreal height, QString text, QGraphicsItem *parent = nullptr);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem */*option*/, QWidget */*widget*/) override;
    void hoverEnterEvent(QGraphicsSceneHoverEvent *) override;
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *) override;

	enum {Type = UserType + static_cast<int>(GraphicsItemType::kCeil)};

    int type() const override
    {
        return Type;
    }

    QString mText;

    void initializeBrushes();

private:

	enum Marked
	{
		kNone = 0,
		kCross,
		kHigh,
		kLow,
		kSelected,
	};

    bool mInitializedBrushes = false;

    QBrush mEmptyBrush;
    QBrush mEmptyHoverBrush;
    QBrush mHighBrush;
    QBrush mHighHoverBrush;
    QBrush mLowBrush;
    QBrush mLowHoverBrush;
    QBrush mSelectedBrush;
    QBrush mSelectedHoverBrush;
    Marked mMarked = kNone;
    bool mHover = false;
};

#endif // SQUARECEIL_H
