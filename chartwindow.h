#ifndef CHARTWINDOW_H
#define CHARTWINDOW_H

#include "bar.h"
#include "qcustomplot.h"

#include <QMainWindow>

enum ClassType
{
    CPFinancialType
};

namespace Ui {
class ChartWindow;
}

class ChartWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ChartWindow(QWidget *parent = nullptr);
    ~ChartWindow();

    void drawChart(QString title, QVector<Bar> bars);
    void save(QDataStream &stream);

    void parse(QString title, QDataStream &stream);

    const QString &getFile() const;
    void setFile(const QString &newFile);

public slots:
    void contextMenuRequest(const QPoint &pos);
    void setProperties();

private:
    Ui::ChartWindow *ui;

    QString file;
};

#endif // CHARTWINDOW_H
