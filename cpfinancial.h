#ifndef CPFINANCIAL_H
#define CPFINANCIAL_H

#include "bar.h"
#include "qcustomplot.h"
#include "colorselectwindow.h"

class CPFinancial : public QCPFinancial
{
    enum ColorOrder
    {
        BodyPositive,
        BorderPositive,
        BodyNegative,
        BorderNegative,
        BodySelected,
        BorderSelected,

        ColorsCount
    };

    Q_OBJECT

    friend QDataStream &operator<<(QDataStream &, const CPFinancial &);
    friend QDataStream &operator>>(QDataStream &, CPFinancial &);

public:
    CPFinancial(QString title, QVector<Bar> &bars,  QCPAxis *keyAxis, QCPAxis *valueAxis, NamesAndColors colors = NamesAndColors());
    NamesAndColors &getColorsData();
    virtual ~CPFinancial();

    void trendLine(int n);

public slots:
    void repaint();
    void rescaleY(const QCPRange &range);

private:
    NamesAndColors colorsData;
    QVector<Bar> bars;
};

QDataStream &operator<<(QDataStream &, const CPFinancial &);
QDataStream &operator>>(QDataStream &, CPFinancial &);

#endif // CPFINANCIAL_H
