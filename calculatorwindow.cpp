#include "calculatordialog.h"
#include "calculatorwindow.h"
#include "ui_calculatorwindow.h"

#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <qmath.h>

#include "calculatorscene.h"

CalculatorWindow::CalculatorWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::CalculatorWindow)
{
    ui->setupUi(this);

    mScene = QSharedPointer<CalculatorScene>(new CalculatorScene);
    ui->calculatorView->setScene(mScene.get());
    ui->calculatorView->setRenderHints( QPainter::Antialiasing | QPainter::SmoothPixmapTransform );
    connect(ui->actionSettings, &QAction::triggered, this, &CalculatorWindow::settingsWindow);
    connect(ui->actionSaveAsImage, &QAction::triggered, this, &CalculatorWindow::saveAsImage);

    connect(ui->actionHighLowMarks, &QAction::toggled, this, [this](bool checked)
    {
        ui->actionMarkHigh->setVisible(checked);
        ui->actionMarkLow->setVisible(checked);
        ui->actionMarkSelected->setVisible(checked);

        mScene->changeMode(CalculatorScene::kMarkerAny);
    });

    connect(ui->actionMarkHigh, &QAction::toggled, this, [this](bool checked)
    {
        ui->actionMarkLow->setDisabled(checked);
        ui->actionMarkSelected->setDisabled(checked);

        mScene->changeMode(checked ? CalculatorScene::kMarkerHigh : CalculatorScene::kMarkerAny);
    });

    connect(ui->actionMarkLow, &QAction::toggled, this, [this](bool checked)
    {
        ui->actionMarkHigh->setDisabled(checked);
        ui->actionMarkSelected->setDisabled(checked);

        mScene->changeMode(checked ? CalculatorScene::kMarkerLow : CalculatorScene::kMarkerAny);
    });

    connect(ui->actionMarkSelected, &QAction::toggled, this, [this](bool checked)
    {
        ui->actionMarkLow->setDisabled(checked);
        ui->actionMarkHigh->setDisabled(checked);

        mScene->changeMode(checked ? CalculatorScene::kMarkerSelected : CalculatorScene::kMarkerAny);
    });

    connect(ui->actionHighLowMarks, &QAction::toggled, this, [this](bool checked)
    {
        if(!checked)
        {
            ui->actionMarkHigh->setChecked(false);
            ui->actionMarkLow->setChecked(false);
            ui->actionMarkSelected->setChecked(false);
        }

        mScene->changeMode(checked ? CalculatorScene::kMarkerAny : CalculatorScene::kFree);
    });

    connect(ui->actionSetColors, &QAction::triggered, this, &CalculatorWindow::setColors);
}

CalculatorWindow::~CalculatorWindow()
{
    delete ui;
}

void CalculatorWindow::settingsWindow()
{
    CalculatorDialog *dlg = new CalculatorDialog(mScene->type(), mScene->clockwise(), mScene->size(), this);
    dlg->setAttribute(Qt::WA_DeleteOnClose);
    connect(dlg, &CalculatorDialog::sizeChanged, mScene.get(), &CalculatorScene::sizeChanged);
    connect(dlg, &CalculatorDialog::directionChanged, mScene.get(), &CalculatorScene::directionChanged);
    connect(dlg, &CalculatorDialog::typeChanged, mScene.get(), &CalculatorScene::typeChanged);
    connect(dlg, &CalculatorDialog::rebuild, mScene.get(), &CalculatorScene::rebuild);
    ui->actionSettings->setDisabled(true);
    connect(dlg, &CalculatorDialog::finished, this, [this](){ui->actionSettings->setDisabled(false);});
    dlg->show();
}

void CalculatorWindow::saveAsImage()
{
    QString fileName = QFileDialog::getSaveFileName(this, QString("Save as: "),
                                                QDir::currentPath().append(QDir::separator()).append("image"), "*.png");
    if(fileName.size())
    {
        QFile inputFile(fileName);
        if (inputFile.open(QIODevice::WriteOnly))
        {
            QPixmap pm = ui->calculatorView->viewport()->grab(ui->calculatorView->viewport()->rect());
            pm.save(fileName);
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText(inputFile.errorString());
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.exec();
        }

        inputFile.close();
    }
}

void CalculatorWindow::setColors()
{
    ColorSelectWindow *pw = new ColorSelectWindow(mScene->getColors(), this);
    pw->setAttribute(Qt::WA_DeleteOnClose);
    connect(pw, &ColorSelectWindow::repaintParent, mScene.get(), &CalculatorScene::recolor);
    pw->show();
}
