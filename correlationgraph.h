#ifndef CORRELATIONGRAPH_H
#define CORRELATIONGRAPH_H

#include <QWidget>

#include "correlation.h"

namespace Ui {
class CorrelationGraph;
}

class CorrelationGraph : public QWidget
{
    Q_OBJECT

public:
    explicit CorrelationGraph(QVector<Correlation> data_, QWidget *parent = nullptr);
    ~CorrelationGraph();

private:
    Ui::CorrelationGraph *ui;

    QVector<Correlation> data;
};

#endif // CORRELATIONGRAPH_H
